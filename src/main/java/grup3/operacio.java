
package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class operacio {
     private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
     private final SimpleStringProperty nom = new SimpleStringProperty();
     
      public int getCodi() {
        return codi.get();
    }

    public SimpleIntegerProperty codiProperty() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi.set(codi);
    }

    //
    public String getNom() {
        return nom.get();
    }

    public SimpleStringProperty nomProperty() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom.set(nom);
    }
    
}
