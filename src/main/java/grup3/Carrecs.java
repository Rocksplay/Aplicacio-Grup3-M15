package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author m15
 */
public class Carrecs {
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
    private final SimpleStringProperty nom = new SimpleStringProperty();
    //
    public Integer getCodi() {
        return codi.get();
    }
    public SimpleIntegerProperty CodiProperty() {
        return codi;
    }
    public void setCodi(Integer codi) {
        this.codi.set(codi);
    }
    //
    public String getNom() {
        return nom.get();
    }
    public SimpleStringProperty nomProperty() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom.set(nom);
    }
 
}