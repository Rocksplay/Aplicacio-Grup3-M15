package grup3;

import grup3.login.FXMLAltaAdrecesController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author infot
 */
public class ProvinciesHelper {

    public static Provincies obteProvincies(String nom, Connection connection) {
        Provincies provincia = new Provincies();
        try {
            PreparedStatement consulta = connection.prepareStatement("SELECT id, nom, id_pais FROM provincia WHERE nom = ?;");
            consulta.setString(1, nom);
            ResultSet resultado = consulta.executeQuery();

            if (resultado.next()) {
                provincia.setId(resultado.getInt(1));
                provincia.setNom(resultado.getString(2));
                provincia.setPais(resultado.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return provincia;
    }
}
