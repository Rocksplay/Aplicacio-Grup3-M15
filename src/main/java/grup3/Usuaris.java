/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author m15
 */
public class Usuaris {

    private final SimpleStringProperty nom = new SimpleStringProperty();
    private final SimpleStringProperty contrasenya = new SimpleStringProperty();
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
    private final SimpleIntegerProperty bloquejat = new SimpleIntegerProperty();
    private final SimpleStringProperty data_contrasenya = new SimpleStringProperty();
    private final SimpleStringProperty cognom = new SimpleStringProperty();
    private final SimpleStringProperty email = new SimpleStringProperty();
    private final SimpleIntegerProperty admin = new SimpleIntegerProperty();
    private final SimpleStringProperty usuari = new SimpleStringProperty();

    public String getNom() {
        return nom.get();
    }

    public SimpleStringProperty nomProperty() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom.set(nom);
    }

    //
    public String getContrasenya() {
        return contrasenya.get();
    }

    public SimpleStringProperty contrasenyaProperty() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya.set(contrasenya);
    }

    //
    public Integer getCodi() {
        return codi.get();
    }

    public SimpleIntegerProperty codiProperty() {
        return codi;
    }

    public void setCodi(Integer codi) {
        this.codi.set(codi);
    }

    //
    public Integer getBloquejat() {
        return bloquejat.get();
    }

    public SimpleIntegerProperty bloquejatProperty() {
        return bloquejat;
    }

    public void setBloquejat(Integer bloquejat) {
        this.bloquejat.set(bloquejat);
    }

    //
    public String getData_contrasenya() {
        return data_contrasenya.get();
    }

    public SimpleStringProperty data_contrasenyaProperty() {
        return data_contrasenya;
    }

    public void setData_contrasenya(String data_contrasenya) {
        this.data_contrasenya.set(data_contrasenya);
    }

    //
    public String getCognom() {
        return cognom.get();
    }

    public SimpleStringProperty cognomProperty() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom.set(cognom);
    }

    //
    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    //
    public Integer getAdmin() {
        return admin.get();
    }

    public SimpleIntegerProperty adminProperty() {
        return admin;
    }

    public void setAdmin(Integer admin) {
        this.admin.set(admin);
    }

    //
    public String getUsuari() {
        return usuari.get();
    }

    public SimpleStringProperty usuariProperty() {
        return usuari;
    }

    public void setUsuari(String usuari) {
        this.usuari.set(usuari);
    }
}
