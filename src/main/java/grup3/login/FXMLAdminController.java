package grup3.login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import grup3.ConnectionUtil;
import grup3.classes.Utilitats;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author m15
 */
public class FXMLAdminController implements Initializable {

    //VARIABLES
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    
    final String UPDATE = "UPDATE admin SET "
                                + "intents_bloqueig = ? ,"
                                + "caducitat_contrasenya = ? , " 
                                + "email_admin = ? , "
                                + "nom_admin = ? "
                         + " WHERE codi = ?";

    final Integer ID_ADMIN = 1;
    
    public FXMLAdminController() {
        connection = ConnectionUtil.connectdb();
    }

    @FXML
    private void buttonOnAction(ActionEvent event) {
        if (!email_admin.getText().contains("@")) {
            Utilitats.alerta_err("Error al guardar les dades", "Error al guardar les dades", "Email incorrecte");
        } else {
            actualitzaConfig();
            Utilitats.alerta_info("Informació","","Dades actualitzades correctament");
            Stage stageAdmin = (Stage) intents.getScene().getWindow();
            stageAdmin.close();
        }
    }
    
    private void actualitzaConfig() {        
        try {
            PreparedStatement ps = connection.prepareStatement(UPDATE);
            ps.setInt(1, Integer.parseInt(intents.getText()));
            ps.setInt(2, Integer.parseInt(caducitat.getText()));            
            ps.setString(3, email_admin.getText());
            ps.setString(4, nom_admin.getText());
            ps.setInt(5, ID_ADMIN);
            ps.executeUpdate();
        } catch (NumberFormatException ne) {
        
        } catch (SQLException ex) {
            
        }
        
    }
    @FXML
    private void onActionSortir(ActionEvent event){
            Stage stageAdmin = (Stage) intents.getScene().getWindow();
            stageAdmin.close();
    }
    @FXML
    protected void intentsTY(KeyEvent event) {
        if (!event.getCharacter().matches("[0-9]")) {
            event.consume();
        }
    }

    @FXML
    protected void mesosTY(KeyEvent event) {
        if (!event.getCharacter().matches("[0-9]")) {
            event.consume();
        }
    }

    @FXML
    TextField email_admin, intents, caducitat, nom_admin;

    public void initialize(URL url, ResourceBundle rb) {
        try {
            String sql = "SELECT * FROM admin WHERE codi = 1";
            PreparedStatement consulta = connection.prepareStatement(sql);
            ResultSet resultado = consulta.executeQuery();

            while (resultado.next()) {
                String email = resultado.getString("email_admin");
                String nom_administrador = resultado.getString("nom_admin");
                int caducitat_contrasenya = resultado.getInt("caducitat_contrasenya");
                int intents_bloqueig = resultado.getInt("intents_bloqueig");

                email_admin.setText(email);
                intents.setText(String.valueOf(intents_bloqueig));
                caducitat.setText(String.valueOf(caducitat_contrasenya));
                nom_admin.setText(nom_administrador);

            }
        } catch (Exception e) {
        }

    }

}
