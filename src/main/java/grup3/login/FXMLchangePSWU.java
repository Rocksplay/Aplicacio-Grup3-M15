/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3.login;

import grup3.classes.Utilitats;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import static grup3.login.FXMLLoginController.connection;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class FXMLchangePSWU implements Initializable {

    //VARIABLES
   @FXML TextField idNewPas;
   @FXML TextField idRepPas;
   
   private String usuari;
   //public static String nomtr;

   final String UPDATE = "UPDATE login SET "
                                + "contrasenya = ? "
                         + " WHERE usuari = ?";
   
   public void setUsuari (String usuari) {
       this.usuari = usuari;
   }
   
    //PROGRAMA
    private static String encriptar(String s) throws UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }

    @FXML
    private void buttonOnAction(ActionEvent event) throws UnsupportedEncodingException, SQLException, Exception {
        if (idNewPas.getText().equals(idRepPas.getText())) {
             FXMLUsuarisController Usuaris = new FXMLUsuarisController();
             //nomtr = Usuaris.nom;
        
        Utilitats.alerta_info("Dades Guardades", "DADES", "Dades guardades correctament");
              PreparedStatement update = connection.prepareStatement(UPDATE);
               update.setString(1, encriptar(idNewPas.getText()));
               update.setString(2, Usuaris.nom);
               update.executeUpdate();
        }else{
            Utilitats.alerta_err("Error", "Error", "Error al guardar les dades ");
            idNewPas.setText("");
            idRepPas.setText("");
            idNewPas.setFocusTraversable(true);
            
        }
    }

    public void initialize(URL url, ResourceBundle rb) {


    }
   
}
