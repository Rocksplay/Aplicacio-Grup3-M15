package grup3.login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import grup3.Tipus_impost;
import static grup3.login.FXMLLoginController.connection;
import static grup3.login.FXMLUsuarisController.POSICIO;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class FXMLtipus_impostController implements Initializable {
     String UPDATE = "UPDATE tipus_impost SET "
                                + "nom = ?, valor = ?  "
                                + "WHERE nom = ?";
    
    String INSERT = "INSERT INTO tipus_impost VALUES(?,?,?)";
    
    ObservableList <Tipus_impost> llistaImpost = FXCollections.observableArrayList();
    
    Tipus_impost impost = new Tipus_impost();
    
    int accio = 0;

    int contador;
    
    String code;
    
    String nom_impost;
    
    String valor;
    
   @FXML
    private TableColumn<Tipus_impost, Integer> codi;
   
    @FXML
    private TableColumn<Tipus_impost, String> Tipus;
    
    @FXML
    private TableColumn<Tipus_impost, Integer> Valor;
    
    @FXML
    private TableView<Tipus_impost> tvImpost;
    
    @FXML
    private Button btnFirst;
    
    @FXML
    private Button btnDelete;
    
    @FXML
    private Button btnGuardar;
    
    @FXML
    TableColumn colCodi;
    
    @FXML
    TableColumn colTipus;
    
    @FXML
    TableColumn colValor;
    
    @FXML
    TextField tfCodi;
    
    @FXML
    TextField tfImpost;
    
    @FXML
    TextField tfValor;
    
    @FXML 
    Button btnLeft;
    @FXML 
    Button btnRight;
    @FXML 
    Button btnLast;
    
    @FXML
    Button btnCancelar;
    
    @FXML
    private void guardarOnAction() throws SQLException {
            if(accio==1){
            //ALERT
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Guardar i Sortir");
            alert.setHeaderText(null);
            alert.setContentText("Dades Guardades Correctament");
            alert.showAndWait();            
      
            code=tfCodi.getText(); 
            nom_impost=tfImpost.getText();
            valor=tfValor.getText();
            
            PreparedStatement con1 = connection.prepareStatement(INSERT);
            con1.setInt(1, contador + 1);
            con1.setString(2, nom_impost);
            con1.setInt(3, Integer.parseInt(valor));
            
            con1.execute();   
           
            }
            else{
                modificar();
            }
            
            refresca();
            
            tfCodi.setEditable(true);
            tfCodi.setDisable(false);
            btnCancelar.setVisible(false);
            activaMov();
            buida();
    }
    
    @FXML
    private void sortirOnAction(ActionEvent event) throws SQLException {
        Stage stageAdmin = (Stage) btnGuardar.getScene().getWindow();
        stageAdmin.close();
    }
    
    @FXML
    private void deleteOnAction(ActionEvent event) throws SQLException {
        FXMLLoginController login = new FXMLLoginController();
        Tipus_impost t = tvImpost.getItems().get(tvImpost.getSelectionModel().getFocusedIndex());
        
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmar eliminar");
            alert.setHeaderText("Eliminar");
            alert.setContentText("Vols eliminar el tipus d'impost: " + t.getNom()+ " ?");

            ButtonType btnSi = new ButtonType("Si");
            ButtonType btnNo = new ButtonType("No");tvImpost.getItems().get(tvImpost.getSelectionModel().getFocusedIndex());

            alert.getButtonTypes().setAll(btnSi, btnNo);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == btnSi) {
                eliminarImpost(t.getNom());
                refresca();
            }
    }
    
    @FXML
    private void nouImpostAction(ActionEvent event) throws SQLException {
        accio = 1;
        
        tfCodi.setDisable(true);
        btnCancelar.setVisible(true);
        desacMov();
        
        tfCodi.setText("");
        tfImpost.setText("");
        tfValor.setText("");
        
         // CONTADOR FILES PER CODI                
         String select_files = "SELECT * FROM tipus_impost";
         PreparedStatement con2 = connection.prepareStatement(select_files);
         ResultSet resultado1 = con2.executeQuery();

        while (resultado1.next()) {
            contador = resultado1.getInt("codi");
        }
            
        tfCodi.setText(String.valueOf(contador+1));
        
        tfImpost.requestFocus();
        
        tfImpost.setOnKeyPressed(evento -> {
            
            if (evento.getCode().equals(KeyCode.ENTER)){
            
                tfValor.requestFocus();
               
            }
        });
        
        tfValor.setOnKeyPressed(evento -> {
            
            if (evento.getCode().equals(KeyCode.ENTER)){
            
                btnGuardar.requestFocus();
               
            }
        });
         
        btnGuardar.setOnKeyPressed(evento -> {
            if (evento.getCode().equals(KeyCode.ENTER) || evento.getCode().equals(KeyCode.TAB)) {
                 try {
                    guardarOnAction();
                } catch (Exception ex) {
                    Logger.getLogger(FXMLCrearUsuariController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }); 
    }
    
    @FXML
    private void btnPrimerOnAction (ActionEvent event) {
        POSICIO = 0;
        if(tvImpost.getFocusModel().getFocusedIndex() != POSICIO){  
            tvImpost.requestFocus();
            tvImpost.getSelectionModel().select(POSICIO);
            tvImpost.getFocusModel().focus(POSICIO);
            mostrar_dades();
            accio=2;
        }
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
       int TOTAL =  tvImpost.getItems().size();
       if(POSICIO == TOTAL - 1){
           POSICIO = TOTAL;
           POSICIO= tvImpost.getFocusModel().getFocusedIndex();
           mostrar_dades();
           accio=2;
       }
       else {
            POSICIO= tvImpost.getFocusModel().getFocusedIndex();
            POSICIO++;
            tvImpost.requestFocus();
            tvImpost.getSelectionModel().select(POSICIO);
            tvImpost.getFocusModel().focus(POSICIO);
            mostrar_dades();
            accio=2;
        }
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        int TOTAL =  tvImpost.getItems().size()-1;
        int PRIMER = 0;
        if (POSICIO < PRIMER) {
            POSICIO = PRIMER;
            mostrar_dades();
            accio=2;
        
        }
        else {
            POSICIO= tvImpost.getFocusModel().getFocusedIndex();
            POSICIO--;
            tvImpost.requestFocus();
            tvImpost.getSelectionModel().select(POSICIO);
            tvImpost.getFocusModel().focus(POSICIO);
            mostrar_dades();
            accio=2;
        }
    }
    
    @FXML
    private void btnUltimOnAction (ActionEvent event) {
        int TOTAL =  tvImpost.getItems().size()-1;
        if(tvImpost.getFocusModel().getFocusedIndex() != TOTAL){  
            tvImpost.requestFocus();
            tvImpost.getSelectionModel().select(TOTAL);
            tvImpost.getFocusModel().focus(TOTAL);
            mostrar_dades();
            accio=2;
        }
    }
    
    @FXML
    private void cancelaAction() {
        tvImpost.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();
        refresca();
        buida();
    }
   
    private void mostrar_dades(){
        try {
            String sql = "SELECT codi, nom, valor FROM tipus_impost WHERE nom = ?";
            PreparedStatement consulta = connection.prepareStatement(sql);
            consulta.setString(1, nom_impost);

            ResultSet resultado = consulta.executeQuery();

            if (resultado.next()) {
                int codi = resultado.getInt("codi");
                String imposts = resultado.getString("nom");
                int valorIm = resultado.getInt("valor");

                tfCodi.setText(String.valueOf(codi));
                tfImpost.setText(imposts);
                tfValor.setText(String.valueOf(valorIm));
                
                nom_impost=tfImpost.getText();
            }

        } catch (Exception e) {
        
        }
        
    }
    
        private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }
    
     private void buida() {
        tfCodi.setText("");
        tfValor.setText("");
        tfImpost.setText("");
    }
    
    private void modificar(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Confirmació de modificació");
        alert.setHeaderText(null);
        alert.setContentText("Dades modificades correctament!");
        alert.showAndWait();
    
        PreparedStatement preparedStatement;
        try {
            System.out.println(nom_impost);
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1,tfImpost.getText());
            preparedStatement.setInt(2, Integer.parseInt(tfValor.getText()));
            preparedStatement.setString(3,nom_impost);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut modificar l'impost", ex);            
        }  
    }
    
    private void eliminarImpost(String Impost) {                
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM tipus_impost WHERE nom = ?");
            preparedStatement.setString(1,Impost);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut eliminar l'impost...", ex);            
        }        
    }
    
    private ObservableList<Tipus_impost> obtenirDades() {        
        String sql = "SELECT codi, nom, valor FROM tipus_impost";
        ArrayList<Tipus_impost> list = new ArrayList<>();        

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Tipus_impost t = new Tipus_impost();
                t.setCodi(resultSet.getInt(1));
                t.setNom(resultSet.getString(2));
                t.setValori(resultSet.getInt(3));
                list.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ObservableList<Tipus_impost> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }
    
    private void refresca() {
        llistaImpost = obtenirDades();
        tvImpost.setItems(llistaImpost);
        
        //taulaFiltratge();
    }
    
    private void configuraColumnes(){
            colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
            colTipus.setCellValueFactory(new PropertyValueFactory<>("nom"));
            colValor.setCellValueFactory(new PropertyValueFactory<>("valor"));
        
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     configuraColumnes();
        refresca();
        
        //Doble click
        tvImpost.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                btnDelete.setDisable(false);
                nom_impost = newValue.getNom();
                tvImpost.setOnMousePressed(event -> {
                    if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                        mostrar_dades();
                        tfCodi.setEditable(false);
                        tfCodi.setDisable(true);
                        accio=2;
                    }
                });
            } else {
                btnDelete.setDisable(true);
            }
        });
    }        
    
}
