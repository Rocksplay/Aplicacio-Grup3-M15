package grup3.login;

import grup3.ConnectionUtil;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.BooleanBinding;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.StageStyle;

/**
 * /**
 *
 * @author GRUP 3
 */
public class FXMLLoginController implements Initializable {

    ////////////////////////////////VARIABLES////////////////////////////////////////
    int i = 0;
    int j = 0;
    int blo;
    int intents_de_bloquejat;
    public static String passw;
    public static String usr;
    public static String ip;
    public static int Admin;
    public static boolean page;
    public static int codi;
    static String email_administrador = ("grup3m15@gmail.com");
    static String contrasenya_mail = ("ZXVnZW5pMTg=");
    static int canviar_contrasenya = 0;
    FXMLchangePSW changepsw = new FXMLchangePSW();
    FXMLMenuController menu = new FXMLMenuController();
    
    //CONNEXIO
    static Connection connection = null;
    static PreparedStatement preparedStatement = null;
    static ResultSet resultSet = null;
    
    //ESCENA
   // Stage dialogStage = new Stage();
    //Scene scene;
    
    @FXML
    private Button button;

    @FXML
    private Label lbBloqueig;

    @FXML
    private TextField textUsuari;

    @FXML
    private PasswordField textPassword;
    
    @FXML
    private void onMouseClicked(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY) {
            tancaApp();
        }
    }
    
    @FXML
    private void infoAction(ActionEvent event) throws IOException {
        //scene = new Scene(FXMLLoader.load(getClass().getResource("/view/FXMLinfo.fxml")));
        //dialogStage.setScene(scene);
        //dialogStage.show();
        
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLinfo.fxml"));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.getIcons().add(new Image ("file:icono.png"));
            stage.setScene(new Scene(root));
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    @FXML
    private void buttonOnAction(ActionEvent event) {
        login(textUsuari.getText(), textPassword.getText());
    }

    ////////////////////////////////////////////FUNCIONS////////////////////////////////////////////
    public FXMLLoginController() throws SQLException {
        connection = ConnectionUtil.connectdb();
    }
    
    //CLOSE CONNECTION
    public static void tancaConnexio(Connection con) throws SQLException{
        con.close();
    }
    
    //DESXIFRAR CONTRASENYA USUARI
    public static String xifrar(String psw) throws UnsupportedEncodingException {

        String Desencrypt = desencriptar(psw);

        return Desencrypt;
    }
    
    //DESXIFRAR CONTRASENYA USUARI
    private static String desencriptar(String s) throws UnsupportedEncodingException {
        byte[] decode = java.util.Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    }

    //EMAIL
    public static void enviarmail(String user) {
        try {
            
            
            
            // Propiedades de la conexión
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "grup3m15@gmail.com");
            props.setProperty("mail.smtp.auth", "true");
            
            

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);
            

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
           
            message.setFrom(new InternetAddress("yo@yo.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(email_administrador));
  
            
            message.setSubject("Intrús");
            message.setText("Hi ha hagut 3 intents incorrectes amb l'usuari: " + user);

                        

            
            contrasenya_mail = xifrar(contrasenya_mail);
            
            

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect(email_administrador, contrasenya_mail);
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    //TANCA L'APP
    private void tancaApp() {
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
    }
    //Obtenir IP del servidor a connectar
    private void ipServer(Connection con) throws SQLException{
        String url = con.getMetaData().getURL();
            
            String[] parts = url.split("/");

            ip= parts[2];
    }
    
    //CONNEXIÓ D'USUARI AMB LES COMPROVACIONS DE CONTRASSENYES(USUARI BLOQUEJAT-CADUCITAT DE LA PSW...)
    private void login(String username, String password) {
        //String username = textUsuari.getText().toString();
        //String password = textPassword.getText().toString();
        canviar_contrasenya = 0;

        String sql = "SELECT * FROM login WHERE usuari = ? and contrasenya = ?";
        
        try {
            //Obtenir IP del Servidor 
            ipServer(connection);
            
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);

            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            String query = "select * from login where usuari = ?";
            PreparedStatement consulta = connection.prepareStatement(query);
            consulta.setString(1, username);
            ResultSet resultado = consulta.executeQuery();

            while (resultado.next()) {
                String psw = resultado.getString("contrasenya");
                codi = resultado.getInt("codi");
                Admin=resultado.getInt("admin");
                passw = xifrar(psw);

            }

            try {
                if (passw.equals(password)) {

                    String validar = "select * from login where usuari = ?";
                    PreparedStatement consulta3 = connection.prepareStatement(validar);
                    consulta3.setString(1, username);
                    ResultSet resultado2 = consulta3.executeQuery();

                    //Funció per canviar de contrassenya quan es caduca
                    changepsw.agafar_username(username);

                    while (resultado2.next()) {
                        blo = resultado2.getInt("bloquejat");
                    }

                    if (blo == 0) {
                        
                        // CADUCITAT CONTRASENYES
                        // CONTRASENYA CADUCITAT 
                        String query4 = "select * from login where usuari = ?";
                        PreparedStatement consulta4 = connection.prepareStatement(query4);
                        consulta4.setString(1, username);
                        ResultSet resultado4 = consulta4.executeQuery();
                        while (resultado4.next()) {
                            Date data_usuari = resultado4.getDate("data_contrasenya");
                            
                            // DATA ACTUAL 
                            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
                            Date now = new Date();
                            String data_servidor = sdfDate.format(now);
                            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            Date data_actual = format.parse(data_servidor);

                            //CANVIAR CONTRASENYA
                            if (data_actual.after(data_usuari)) {
                                canviar_contrasenya = 1;
                            }
                        }

                        if (canviar_contrasenya == 1) {
                            infoBox("La contrasenya de l'usuari " + username + " ha caducat.", "Error", null);
                            
                            //MOSTRA CHANGE PASSWORD                      
                            try {
                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLchangePsw.fxml"));
                                Parent root = (Parent) loader.load();
                                Stage stage = new Stage();
                                stage.setTitle("Canvi de contrasenya - Grup3");
                                stage.initModality(Modality.APPLICATION_MODAL);
                                stage.initStyle(StageStyle.DECORATED);
                                stage.getIcons().add(new Image ("file:icono.png"));
                                stage.setScene(new Scene(root));
                                stage.show();                                

                            } catch (IOException ex) {
                                Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else {

                            infoBox("Accés correcte!!", "Login", null);
                            
                            usr = username;
                            passw= password;
                            
                                //Tancar login al cridar el menu
                                Stage tnc = (Stage) button.getScene().getWindow();
                                tnc.close();
                                //Cridar al menu
                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLMenu.fxml"));
                                Parent root = (Parent) loader.load();
                                Stage stage = new Stage();
                                stage.setTitle("Menú - Grup3");
                                stage.setResizable(true);
                                stage.initStyle(StageStyle.DECORATED);
                                stage.getIcons().add(new Image ("file:icono.png"));
                                stage.setMaximized(true);
                                stage.setScene(new Scene(root));
                                stage.show();

                            
                        }

                    } else {
                        infoBox("L'usuari "+username+" està bloquejat", "Error", null);
                    }

                } else {
                    infoBox("Usuari o contrassenya incorrecte! ", "Error", null);
                    reset();

                    // INTENTS BLOQUEIG
                    String query5 = "select * from admin where codi = ?";
                    PreparedStatement consulta5 = connection.prepareStatement(query5);
                    consulta5.setInt(1, 1);
                    ResultSet resultado5 = consulta5.executeQuery();
                    while (resultado5.next()) {
                        int intents_bloquejat = resultado5.getInt("intents_bloqueig");
                        intents_de_bloquejat = intents_bloquejat;
                    }

                    if (i == intents_de_bloquejat - 1) {
                        infoBox("S'ha bloquejat l'usuari: " + username, "Failed", null);
                        enviarmail(username);

                        String bloquejar = "UPDATE login SET bloquejat = ? WHERE usuari = ?";
                        PreparedStatement consulta2 = connection.prepareStatement(bloquejar);
                        consulta2.setInt(1, 1);
                        consulta2.setString(2, username);
                        consulta2.executeUpdate();
                    }
                    i++;
                }

            } catch (Exception el) {
                infoBox("Usuari o contrassenya incorrecte! ", "Error", null);
                el.printStackTrace();
                reset();

                if (j == 2) {
                    infoBox("INTRÚS!!!", "Failed", null);
                    enviarmail(username);

                }
                j++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void infoBox(String infoMessage, String titleBar, String headerMessage) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }

    public void reset() {
        textPassword.setText("");
        textUsuari.setText("");
        textUsuari.requestFocus();
    }
    
    ////////////////////////////////////////////PROGRAMA////////////////////////////////////////////
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        textUsuari.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                textPassword.requestFocus();
            }
        });                
        textPassword.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                login(textUsuari.getText(), textPassword.getText());
            }
        });
        button.setFocusTraversable(false);
        textUsuari.requestFocus();
        
//          BooleanBinding campsObligatoris = textUsuari.textProperty().isEmpty();
//        button.disableProperty().bind(campsObligatoris);

    }
    
    
}
