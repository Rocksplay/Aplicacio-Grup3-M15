package grup3.login;

import grup3.Provincies;
import grup3.classes.Utilitats;
import static grup3.classes.Utilitats.nTeclat;
import static grup3.login.FXMLLoginController.connection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class FXMLAltaProvinciesController implements Initializable {

    @FXML
    TableView<Provincies> tvPaisos;
    
    @FXML
    private TableColumn<Provincies, Integer> colID;
    
    @FXML
    private TableColumn<Provincies, String> colNom;
    
    @FXML
    private TableColumn<Provincies, String> colPais;
    
    @FXML
    TextField tfBuscar;
    
    @FXML
    TextField tfID;
    
    @FXML
    TextField tfNom;
    
    @FXML
    ComboBox cbPais;
    
    @FXML
    Button btnGuardar;
    
    @FXML
    Button btnNou;
    
    @FXML
    private Button btnCancelar;
    
    @FXML
    private Button btnFirst;
    
    @FXML 
    private  Button btnLeft;
    
    @FXML 
    private Button btnRight;
    
    @FXML 
    private Button btnLast;
    
    
    static int PRIMER = 0;
    static int POSICIO = 0;
    static int contador = 0;
    ObservableList<Provincies> llistaPaisos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
            tfID.setDisable(true);
            configuraColumnes();
            refresca();
            nTeclat(tvPaisos);
            
            //// CARREGAR COMBOBOX AL INCI
            /////////////////////////////////////////////////////////
            try{
                String sql_client= "SELECT * FROM pais ";
            PreparedStatement consulta5 = connection.prepareStatement(sql_client);
            ResultSet resultado5= consulta5.executeQuery();
            
                    while (resultado5.next()) {
                        String carrecs = resultado5.getString("nom");
                        ObservableList<String> list = FXCollections.observableArrayList(carrecs);
                        cbPais.getItems().addAll(list);
                    }
                }catch (SQLException ex){}
            ///////////
            
            
            tvPaisos.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Provincies> observable, Provincies oldValue, Provincies newValue) -> {
            if (newValue != null) {
                
                tfID.setText(String.valueOf(newValue.getId()));
                int id_variable=newValue.getId();
                tfNom.setText(newValue.getNom());

                  /////////////////////////////////////
                  //COMBOBOX PAIS
                  /////////////////////////////////////
                try {
                    String sql_client= "SELECT * FROM pais ";
            PreparedStatement consulta5 = connection.prepareStatement(sql_client);
            ResultSet resultado5= consulta5.executeQuery();
            
                    while (resultado5.next()) {
                        String carrecs = resultado5.getString("nom");
                        ObservableList<String> list = FXCollections.observableArrayList(carrecs);
                        cbPais.getItems().addAll(list);
                        
                        String sql3 = "SELECT nom FROM pais WHERE id = ? ";
                        PreparedStatement consulta3 = connection.prepareStatement(sql3);
                        consulta3.setInt(1, id_variable);
                        ResultSet resultado3 = consulta3.executeQuery();
                        while (resultado3.next()) {
                            String nom2 = resultado3.getString("nom");
                            cbPais.setValue(nom2);
                        }}
                    
                    ///////////////////////////////////
                } catch (SQLException ex) {
                    Logger.getLogger(FXMLAltaProvinciesController.class.getName()).log(Level.SEVERE, null, ex);
                }


            }else{
                    
            }
 
        });
      
        BooleanBinding campsObligatoris = tfNom.textProperty().isEmpty();
        btnGuardar.disableProperty().bind(campsObligatoris);
    }

    private void buida() {
        tfID.setText("");
        tfNom.setText("");
    }
    
    private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }
    
    private void configuraColumnes() {
        colID.setCellValueFactory(new PropertyValueFactory<>("id"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        colPais.setCellValueFactory(new PropertyValueFactory<>("pais"));
    }

    private ObservableList<Provincies> obtenirDades() {
        String sql = "select pro.id, pro.nom, pai.nom from provincia pro,pais pai where pro.id_pais=pai.id";
        ArrayList<Provincies> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Provincies p = new Provincies();
                p.setId(resultSet.getInt(1));
                p.setNom(resultSet.getString(2));
                p.setPais(resultSet.getString(3));
                list.add(p);
                contador++;
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList<Provincies> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }
    @FXML
    private void btnSortirOnAction(ActionEvent event) {
        Stage altaPaisos = (Stage) btnGuardar.getScene().getWindow();
        altaPaisos.close();
        contador = 0;
    }
    
    @FXML
    private void cancelaAction() {
        refresca();
        buida();
        btnCancelar.setVisible(false);
        tvPaisos.setDisable(false);
        activaMov();
    }
    
    @FXML
    private void btnNouPaisOnAction(ActionEvent event) {
        tvPaisos.getSelectionModel().clearSelection();
        tvPaisos.setDisable(true);
        desacMov();
        btnCancelar.setVisible(true);
        tfID.setText(String.valueOf(contador + 1));
        tfNom.setText("");
//        cbPais.setText("");
        contador = 0;
    }
    
    @FXML
    private void btnGuardarOnAction(ActionEvent event) throws SQLException {
        String nou = "INSERT INTO provincia VALUES(?,?,?)";
        PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
        int id =  Integer.valueOf(tfID.getText());
        String nom = tfNom.getText();

         // COMBOBOX CARREC
         String pais_variable=cbPais.getSelectionModel().getSelectedItem().toString();
         String sql_carrec = "SELECT id FROM pais WHERE nom = ? ";
         PreparedStatement consulta = connection.prepareStatement(sql_carrec);
         consulta.setString(1, pais_variable);
         ResultSet resultado = consulta.executeQuery();
         //////////////////////////////////////

        preparedStatement2 = connection.prepareStatement(nou);
        
        preparedStatement2.setInt(1, id);
        preparedStatement2.setString(2, nom);
        
        while (resultado.next()) {
                  int id_pais = resultado.getInt("id");
        preparedStatement2.setInt(3, id_pais);
         }
        preparedStatement2.execute();
        
        tvPaisos.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();
        buida();
//        tfPais.setText("");
        refresca();
        contador = 0;
    }
    
    @FXML
    private void btnEliminarOnAction(ActionEvent event) {
        try {
            String del = "DELETE FROM provincia WHERE id = ?";
            PreparedStatement psDel = connection.prepareStatement(del);
            psDel.setString(1, tfID.getText());
            psDel.executeUpdate();
            refresca();
            contador = 0;
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaProvinciesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void taulaFiltratge() {        
        FilteredList<Provincies> filteredData = new FilteredList<>(tvPaisos.getItems(), p -> true);
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(paisos -> {
                if(newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                String upperCaseFilter = newValue.toUpperCase();
                if(paisos.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;               
                }
                return false;
            });
        });
        SortedList<Provincies> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tvPaisos.comparatorProperty());
        tvPaisos.setItems(sortedData);
    }
    
    private void refresca() {                
        tvPaisos.setItems(obtenirDades());
        taulaFiltratge();
    }
        
    @FXML
    private void btnFirstOnAction (ActionEvent event) {
        Utilitats.nBtnFirst(tvPaisos);
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
        Utilitats.nBtnRight(tvPaisos);
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        Utilitats.nBtnLeft(tvPaisos);    
    }
    
    @FXML
    private void btnLastOnAction (ActionEvent event) {
        Utilitats.nBtnLast(tvPaisos);
    }
}
