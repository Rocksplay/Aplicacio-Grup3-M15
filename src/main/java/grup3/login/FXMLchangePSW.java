/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package grup3.login;

import grup3.ConnectionUtil;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import grup3.login.FXMLLoginController.*;
import static grup3.login.FXMLLoginController.infoBox;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;


public class FXMLchangePSW implements Initializable{
    //VARIABLES
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        static String usuari;
        static String nova_data;
        static String mes_servidor_nou;
        int mesos;
    
    //PROGRAMA
    private static String encriptar(String s) throws UnsupportedEncodingException{
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }
    


     public FXMLchangePSW() {
        connection = ConnectionUtil.connectdb();
    }
    
     private void modificar_psw(String usuari )throws Exception{
         
         String query6 = "select * from admin where codi = ?";
         PreparedStatement consulta6 = connection.prepareStatement(query6);
         consulta6.setInt(1, 1);
          ResultSet resultado6 = consulta6.executeQuery();  
         while (resultado6.next()) { 
        int intents_mesos = resultado6.getInt("caducitat_contrasenya");
        mesos=intents_mesos;
         }
         
         
         
        String contrasenya1 = idNewPas.getText();
        String contrasenya2 = idRepPas.getText();
            if (contrasenya1.equals(contrasenya2)){
                String cadenaEncriptada = encriptar(contrasenya1);

                 String canviar = "UPDATE login SET contrasenya = ? WHERE usuari = ?";
                PreparedStatement consulta = connection.prepareStatement(canviar);
                consulta.setString(1, cadenaEncriptada);
                consulta.setString(2, usuari);
                consulta.executeUpdate();
                
                infoBox("La contrasenya s'ha canviat correctament.", "Error", null);
                
         // DATA ACTUAL 

         SimpleDateFormat yyyy = new SimpleDateFormat("yyyy");
         Date any = new Date();
         String any_servidor2 = yyyy.format(any);
         int any_servidor = Integer.parseInt(any_servidor2);
         
         SimpleDateFormat MM = new SimpleDateFormat("MM");
         Date mes = new Date();
         String mes_servidor2 = MM.format(mes);  
         int mes_servidor = Integer.parseInt(mes_servidor2);
         
         SimpleDateFormat dd = new SimpleDateFormat("dd");
         Date dia = new Date();
         String dia_servidor = dd.format(dia);  
      
               
         if (mes_servidor+mesos>12){
             mes_servidor=(mes_servidor+mesos)-12;
             any_servidor=any_servidor+1;
             
             if (mes_servidor<10){
             String mes_servidor3 = String.valueOf(mes_servidor);
             mes_servidor_nou = ("0"+mes_servidor3);
             }
             
         nova_data = (any_servidor+"-"+mes_servidor_nou+"-"+dia_servidor);
    
         }
         
         else{
          
          mes_servidor=mes_servidor+6;   
          nova_data = (any_servidor+"-"+mes_servidor+"-"+dia_servidor);
   
         }
    
         String canviar_data = "UPDATE login SET data_contrasenya = ? WHERE usuari = ?";
         PreparedStatement consulta2 = connection.prepareStatement(canviar_data);
         consulta2.setString(1, nova_data);
         consulta2.setString(2, usuari);
         consulta2.executeUpdate();
   
                tancaApp();
            }
            
            else{
                  infoBox("No coincideixen les contrasenyes", "Error", null);

            }
            
                
    
}
     
     public void agafar_username( String username){
        usuari = username;
         
     }
     
   private void tancaApp() {
        Stage stage = (Stage) btnCanviar.getScene().getWindow();
        stage.close();            
    }
 
    @FXML
    private TextField idNewPas;
    
    @FXML
    private TextField idRepPas;
    
    @FXML
    private Button btnCanviar;
    
    @FXML
    private void buttonOnAction(ActionEvent event) throws UnsupportedEncodingException, SQLException, Exception{
        modificar_psw(usuari);
    }
    public void initialize(URL url, ResourceBundle rb) {
        idNewPas.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                idRepPas.requestFocus();
            }
        });
        idRepPas.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                try {
                    modificar_psw(usuari);
                } catch (Exception ex) {
                    Logger.getLogger(FXMLchangePSW.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        btnCanviar.setFocusTraversable(false);
        idNewPas.requestFocus();
    }

}


