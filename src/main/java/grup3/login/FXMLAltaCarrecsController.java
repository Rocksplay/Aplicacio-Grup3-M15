package grup3.login;

import grup3.Carrecs;
import grup3.classes.Utilitats;
import static grup3.classes.Utilitats.nTeclat;
import static grup3.login.FXMLLoginController.connection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class FXMLAltaCarrecsController implements Initializable {

    @FXML
    TableView<Carrecs> tvCarrecs;
    @FXML
    private TableColumn<Carrecs, Integer> colCodi;
    @FXML
    private TableColumn<Carrecs, String> colNom;
    @FXML
    TextField tfBuscar;
    @FXML
    TextField tfID;
    @FXML
    TextField tfNom;
    @FXML
    Button btnGuardar;
    @FXML
    Button btnNou;
    @FXML
    Button btnCancelar;
    @FXML 
    Button btnFirst;
    @FXML 
    Button btnLeft;
    @FXML 
    Button btnRight;
    @FXML 
    Button btnLast;
    
    
    static int PRIMER = 0;
    static int POSICIO = 0;
    static int contador = 0;
    ObservableList<Carrecs> llistaCarrecs = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
            tfID.setDisable(true);
            configuraColumnes();
            refresca();
            nTeclat(tvCarrecs);
            
            tvCarrecs.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Carrecs> observable, Carrecs oldValue, Carrecs newValue) -> {
            if (newValue != null) {
                
                tfID.setText(String.valueOf(newValue.getCodi()));
                tfNom.setText(newValue.getNom());
            }
        });
        BooleanBinding campsObligatoris = tfNom.textProperty().isEmpty();
        btnGuardar.disableProperty().bind(campsObligatoris);
    }

    private void configuraColumnes() {
        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
//        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
    }
    
    private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }

    private ObservableList<Carrecs> obtenirDades() {
        String sql = "SELECT codi, nom FROM carrec";
        ArrayList<Carrecs> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Carrecs p = new Carrecs();
                p.setCodi(resultSet.getInt(1));
                p.setNom(resultSet.getString(2));
                list.add(p);
                contador++;
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList<Carrecs> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }
    
    @FXML
    private void cancelaAction() {
        tvCarrecs.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();
        refresca();
        buida();

    }
    
     private void buida() {
        tfID.setText("");
        tfNom.setText("");
    }
    
    @FXML
    private void btnSortirOnAction(ActionEvent event) {
        Stage altaCarrecs = (Stage) btnGuardar.getScene().getWindow();
        altaCarrecs.close();
        contador = 0;
    }
    @FXML
    private void btnNouPaisOnAction(ActionEvent event) {
        tvCarrecs.getSelectionModel().clearSelection();
        tvCarrecs.setDisable(true);
        tfID.setText(String.valueOf(contador + 1));
        tfNom.setText("");
//        tfCodi.setText("");
        contador = 0;
        btnCancelar.setVisible(true);
        desacMov();
    }
    
    @FXML
    private void btnGuardarOnAction(ActionEvent event) throws SQLException {
        String nou = "INSERT INTO carrec VALUES(?,?)";
        PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
        int id =  Integer.valueOf(tfID.getText());
        String nom = tfNom.getText();
        
        preparedStatement2 = connection.prepareStatement(nou);
        
        preparedStatement2.setInt(1, id);
        preparedStatement2.setString(2, nom);
        preparedStatement2.execute();
        
        tvCarrecs.setDisable(false);
        tfID.setText("");
        tfNom.setText("");
        btnCancelar.setVisible(false);
        activaMov();
//        tfCodi.setText("");
        refresca();
        contador = 0;
    }
    
    @FXML
    private void btnEliminarOnAction(ActionEvent event) {
        try {
            String del = "DELETE FROM carrec WHERE codi = ?";
            PreparedStatement psDel = connection.prepareStatement(del);
            psDel.setString(1, tfID.getText());
            psDel.executeUpdate();
            refresca();
            contador = 0;
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaCarrecsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//    private void taulaFiltratge() {        
//        FilteredList<Carrecs> filteredData = new FilteredList<>(tvCarrecs.getItems(), p -> true);
//        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
//            filteredData.setPredicate(Carrecs -> {
//                if(newValue == null || newValue.isEmpty()) {
//                    return true;
//                }
//                String lowerCaseFilter = newValue.toLowerCase();
//                String upperCaseFilter = newValue.toUpperCase();
//                if(Carrecs.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
//                    return true;
////                } else if (Carrecs.getCodi().toUpperCase().indexOf(upperCaseFilter) != -1) {
////                    return true;                 
//                }
//                return false;
//            });
//        });
//        SortedList<Carrecs> sortedData = new SortedList<>(filteredData);
//        sortedData.comparatorProperty().bind(tvCarrecs.comparatorProperty());
//        tvCarrecs.setItems(sortedData);
//    }
    
    private void refresca() {                
        tvCarrecs.setItems(obtenirDades());
//        taulaFiltratge();
    }
        
    @FXML
    private void btnFirstOnAction (ActionEvent event) {
        Utilitats.nBtnFirst(tvCarrecs);
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
        Utilitats.nBtnRight(tvCarrecs);
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        Utilitats.nBtnLeft(tvCarrecs);    
    }
    
    @FXML
    private void btnLastOnAction (ActionEvent event) {
        Utilitats.nBtnLast(tvCarrecs);
    }
}
