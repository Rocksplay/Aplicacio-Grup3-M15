package grup3.login;

import grup3.FormaPagament;
import grup3.classes.Utilitats;
import static grup3.login.FXMLLoginController.connection;
import static grup3.login.FXMLUsuarisController.POSICIO;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author m15
 */
public class FXMLFormapagament implements Initializable {

    @FXML
    private TableColumn<FormaPagament, Integer> colCodi;
    @FXML
    private TableColumn<FormaPagament, String> colNom;
    @FXML
    private TableColumn<FormaPagament, String> colFormula_dies;
    @FXML
    private TableView<FormaPagament> tvForma;

    @FXML
    private TextField tFcodi;
    @FXML
    private TextField tFnom;
    @FXML
    private TextField tFformula;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button btnNew;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnCancelar;  
    @FXML 
    Button btnFirst;
    @FXML 
    Button btnLeft;
    @FXML 
    Button btnRight;
    @FXML 
    Button btnLast;
    
    public static String nom;
    static int contador;
    static boolean insert = true;
    ObservableList<FormaPagament> llistaForma = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configuraColumnes();
        refresca();
        tFcodi.setDisable(true);
        tvForma.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends FormaPagament> observable, FormaPagament oldValue, FormaPagament newValue) -> {
            if (newValue != null) {
                insert = false;

                tFcodi.setText(String.valueOf(newValue.getCodi()));
                tFnom.setText(newValue.getNom());
                tFformula.setText(String.valueOf(newValue.getFormula_dies()));

            } else {
                insert = true;
            }
        });

    }

    private ObservableList<FormaPagament> obtenirDades() {
        contador = 0;
        String sql = "SELECT codi, nom, formula_dies FROM forma_pagament";
        ArrayList<FormaPagament> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                FormaPagament c = new FormaPagament();
                c.setCodi(resultSet.getInt(1));
                c.setNom(resultSet.getString(2));
                c.setFormula_dies(resultSet.getInt(3));

                list.add(c);
                contador++;

            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ObservableList<FormaPagament> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }
    
    private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }

    private void configuraColumnes() {
        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        colFormula_dies.setCellValueFactory(new PropertyValueFactory<>("formula_dies"));
    }

    @FXML
    private void refresca() {
        
        tvForma.setItems(obtenirDades());
    }

    @FXML
    private void newContacte() {
        refresca();
        buida();
        tFcodi.setText(String.valueOf(contador));
        tvForma.setDisable(true);
        btnCancelar.setVisible(true);
        desacMov();
        
        if (insert) {
            if (contador != 0) contador = contador +1;
            tFcodi.setText(String.valueOf(contador));}

    }

    private void buida() {
        tFcodi.setText("");
        tFformula.setText("");
        tFnom.setText("");
    }

    @FXML
    private void guardarAction() throws SQLException {
        if (insert) {
            String nou = "INSERT INTO  forma_pagament VALUES (?,?,?)";
            PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
            preparedStatement2.setInt(1, Integer.valueOf(tFcodi.getText()));
            preparedStatement2.setString(2, tFnom.getText());
            preparedStatement2.setInt(3, Integer.valueOf(tFformula.getText()));
            preparedStatement2.execute();
            refresca();
            btnCancelar.setVisible(false);
            activaMov();

        } else {
            String  update = "UPDATE forma_pagament SET nom=?, formula_dies=? WHERE codi = ?";
            PreparedStatement preparedStatement2 = connection.prepareStatement(update);
            preparedStatement2.setInt(3, Integer.valueOf(tFcodi.getText()));
            preparedStatement2.setString(1, tFnom.getText());
            preparedStatement2.setInt(2, Integer.valueOf(tFformula.getText()));
            preparedStatement2.executeUpdate();
            refresca();
        }

        buida();
        tvForma.setDisable(false);
    }

    @FXML
    private void cancelaAction() {
        buida();
        tvForma.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();

    }
    
    @FXML
    private void sortirOnAction(ActionEvent event) throws SQLException {
        Stage stageAdmin = (Stage) btnGuardar.getScene().getWindow();
        stageAdmin.close();
    }
    @FXML
    private void deleteAction() throws SQLException {
        String delete = "DELETE FROM forma_pagament WHERE codi = ?";
        PreparedStatement preparedStatement3 = connection.prepareStatement(delete);
        preparedStatement3.setString(1, String.valueOf(tFcodi.getText()));
        preparedStatement3.executeUpdate();
        buida();
        refresca();

    }
    @FXML
    private void btnFirstOnAction (ActionEvent event) {
        Utilitats.nBtnFirst(tvForma);
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
        Utilitats.nBtnRight(tvForma);
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        Utilitats.nBtnLeft(tvForma);    
    }
    
    @FXML
    private void btnLastOnAction (ActionEvent event) {
        Utilitats.nBtnLast(tvForma);
    }

}
