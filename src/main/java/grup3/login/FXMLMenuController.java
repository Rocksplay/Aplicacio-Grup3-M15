package grup3.login;

import java.io.IOException;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import java.util.Optional;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.StageStyle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.stage.Modality;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author infot
 */
public class FXMLMenuController implements Initializable{
    //VARIABLES
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    FXMLLoginController login;
    FXMLAdminController configAdmin;
    String  usuari;
    String psw;
    String ip;
    int admin;
    int codi;
    
    @FXML
    Button prova;
    @FXML
    TextField tfUsuariCon;
    @FXML
    TextField tfServer;
    @FXML
    MenuItem mIAdmin;
  
    private void mostrarLogin() throws IOException, SQLException {
        try {
            //Tancar menu al desconnectar 
            Stage mnu = (Stage) prova.getScene().getWindow();
            mnu.close();
            //Cridar al login
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLLogin.fxml"));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Login - Grup3");
            stage.setResizable(false);
            stage.initStyle(StageStyle.UNDECORATED);            
            stage.getIcons().add(new Image ("file:icono.png"));                        
            stage.setScene(new Scene(root));
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void onActionLogout(ActionEvent event) throws IOException, SQLException {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Login - Grup3");
        alert.setHeaderText("Vols tancar la teva sessió?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            mostrarLogin();
        } else {
        }
    }
    @FXML
    private void onActionSurt(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Login - Grup3");
        alert.setHeaderText("Vols sortir del sistema?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.exit(0);
        } else {

        }
    }
    @FXML
    private void adminconfAction(ActionEvent event) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAdmin.fxml"));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();
            stage.setTitle("Administrador - Grup3");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.getIcons().add(new Image ("file:icono.png"));
            stage.setScene(new Scene(root));
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    @FXML
    private void UsuariAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLUsuaris.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Usuaris - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    @FXML
    private void contactesAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLContacte.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Contactes - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
    @FXML
    private void aboutAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLinfo.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Informació - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
    @FXML
    private void paisosAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAltaPaisos.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Alta Països - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
    @FXML
    private void adrecesAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAltaAdreces.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Alta Adreces - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
    @FXML
    private void carrecsAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAltaCarrecs.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Alta Carrecs - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
    @FXML
      private void formesAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLFormapagament.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Formes de Pagament - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }  
      
      @FXML
      private void departamentsAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAltaDepartaments.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Alta Departaments - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    } 
      
      @FXML
      private void impostosAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLtipus_impost.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Tipus Impost - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }  
    
      @FXML
      private void viesAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAltaVies.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Alta Vies- Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }  
      
          @FXML
    private void operacionsAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLoperacions.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Operacions - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
        @FXML
    private void provinciesAction(ActionEvent event) throws IOException {       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLAltaProvincies.fxml"));
        Parent parent = (Parent) loader.load();        
        Stage stage = new Stage();
        stage.setTitle("Alta Províncies - Grup3");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.setScene(new Scene(parent));
        stage.show();        
    }
    
    
    
    @Override
     public void initialize(URL url, ResourceBundle rb) {
        admin=login.Admin;
        if (admin==0){
            mIAdmin.setVisible(false);
        }
        else{
            mIAdmin.setVisible(true);
        }

        try {
           login = new FXMLLoginController();
           configAdmin = new FXMLAdminController();
            //DADES DE CONNEXIÓ, USUARI I SERVIDOR
            usuari = login.usr;
            psw = login.passw;
            codi = login.codi;
            ip = login.ip;
           //PASAR DADES DEPENENT DE LA PGAINA, PRINCIPAL I CONFIG ADMIN
            tfUsuariCon.setText(usuari);
            tfServer.setText(ip);   
        } catch (SQLException ex) {
            Logger.getLogger(FXMLMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
}
