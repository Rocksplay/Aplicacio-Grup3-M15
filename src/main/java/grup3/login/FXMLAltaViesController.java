/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3.login;

import grup3.Usuaris;
import grup3.Vies;
import grup3.classes.Utilitats;
import static grup3.classes.Utilitats.alerta_warn;
import static grup3.login.FXMLLoginController.connection;
import static grup3.login.FXMLUsuarisController.POSICIO;
import static grup3.login.FXMLUsuarisController.nom;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLAltaViesController implements Initializable {

    String UPDATE = "UPDATE via SET "
            + "nom = ?  "
            + "WHERE nom = ?";

    String INSERT = "INSERT INTO via VALUES(?,?)";

    ObservableList<Vies> llistavies = FXCollections.observableArrayList();

    Vies via = new Vies();

    int accio = 0;

    int contador;

    String code;

    String nom_via;

    @FXML
    private TableColumn<Vies, Integer> codi;

    @FXML
    private TableColumn<Vies, String> Tipus;

    @FXML
    private TableView<Vies> tvVies;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnGuardar;

    @FXML
    TableColumn colCodi;

    @FXML
    TableColumn colTipus;

    @FXML
    TextField tfCodi;

    @FXML
    TextField tfVia;

    @FXML
    Button btnCancelar;

    @FXML
    Button btnFirst;

    @FXML
    Button btnLeft;

    @FXML
    Button btnRight;

    @FXML
    Button btnLast;

    @FXML
    private void guardarOnAction() throws SQLException {
        if (accio == 1) {
            //ALERT
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Guardar i Sortir");
            alert.setHeaderText(null);
            alert.setContentText("Dades Guardades Correctament");
            alert.showAndWait();

            code = tfCodi.getText();
            nom_via = tfVia.getText();

            PreparedStatement con1 = connection.prepareStatement(INSERT);
            con1.setInt(1, contador + 1);
            con1.setString(2, nom_via);

            con1.execute();

        } else {
            modificar();
        }

        tfCodi.setDisable(false);
        btnCancelar.setVisible(true);
        activaMov();
        refresca();

        buida();
    }

    @FXML
    private void sortirOnAction(ActionEvent event) throws SQLException {
        Stage stageAdmin = (Stage) btnGuardar.getScene().getWindow();
        stageAdmin.close();
    }

    @FXML
    private void deleteOnAction(ActionEvent event) throws SQLException {
        FXMLLoginController login = new FXMLLoginController();
        Vies v = tvVies.getItems().get(tvVies.getSelectionModel().getFocusedIndex());

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmar eliminar");
        alert.setHeaderText("Eliminar");
        alert.setContentText("Vols eliminar el tipus de via: " + v.getVia() + " ?");

        ButtonType btnSi = new ButtonType("Si");
        ButtonType btnNo = new ButtonType("No");
        tvVies.getItems().get(tvVies.getSelectionModel().getFocusedIndex());

        alert.getButtonTypes().setAll(btnSi, btnNo);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btnSi) {
            eliminarVies(v.getVia());
            refresca();
        }
    }

    @FXML
    private void novaViaAction(ActionEvent event) throws SQLException {
        accio = 1;

        tfCodi.setDisable(true);
        btnCancelar.setVisible(true);
        desacMov();

        buida();

        // CONTADOR FILES PER CODI                
        String select_files = "SELECT * FROM via";
        PreparedStatement con2 = connection.prepareStatement(select_files);
        ResultSet resultado1 = con2.executeQuery();

        while (resultado1.next()) {
            contador = resultado1.getInt("codi");
        }

        tfCodi.setText(String.valueOf(contador + 1));

        tfVia.requestFocus();

        tfVia.setOnKeyPressed(evento -> {

            if (evento.getCode().equals(KeyCode.ENTER)) {

                btnGuardar.requestFocus();

            }
        });

        btnGuardar.setOnKeyPressed(evento -> {
            if (evento.getCode().equals(KeyCode.ENTER) || evento.getCode().equals(KeyCode.TAB)) {
                try {
                    guardarOnAction();
                } catch (Exception ex) {
                    Logger.getLogger(FXMLCrearUsuariController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    @FXML
    private void cancelaAction() {
        buida();
        tvVies.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();
    }

    private void buida() {
        tfCodi.setText("");
        tfVia.setText("");
    }

    private void mostrar_dades() {
        try {
            FXMLUsuarisController Usuaris = new FXMLUsuarisController();
            String sql = "SELECT * FROM via WHERE nom = ? ";
            PreparedStatement consulta = connection.prepareStatement(sql);
            consulta.setString(1, nom);

            ResultSet resultado = consulta.executeQuery();

            if (resultado.next()) {
                int codi = resultado.getInt("codi");
                String vies = resultado.getString("nom");

                tfCodi.setText(String.valueOf(codi));
                tfVia.setText(vies);

                nom_via = tfVia.getText();
            }

        } catch (Exception e) {

        }

    }

    private void desacMov() {
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }

    private void activaMov() {
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }

    private void modificar() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Confirmació de modificació");
        alert.setHeaderText(null);
        alert.setContentText("Dades modificades correctament!");
        alert.showAndWait();

        PreparedStatement preparedStatement;
        try {
            System.out.println(nom_via);
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, tfVia.getText());
            preparedStatement.setString(2, nom_via);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut modificar les vies", ex);
        }
    }

    private void eliminarVies(String Via) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM via WHERE nom = ?");
            preparedStatement.setString(1, Via);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut eliminar la via...", ex);
        }
    }

    private ObservableList<Vies> obtenirDades() {
        String sql = "SELECT codi, nom FROM via";
        ArrayList<Vies> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Vies v = new Vies();
                v.setCodi(resultSet.getInt(1));
                v.setVia(resultSet.getString(2));
                list.add(v);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ObservableList<Vies> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }

    private void refresca() {
        llistavies = obtenirDades();
        tvVies.setItems(llistavies);

        //taulaFiltratge();
    }

    private void configuraColumnes() {

        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
        colTipus.setCellValueFactory(new PropertyValueFactory<>("via"));

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configuraColumnes();
        refresca();

        //Doble click
        tvVies.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                btnDelete.setDisable(false);
                nom = newValue.getVia();
                tvVies.setOnMousePressed(event -> {
                    if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                        mostrar_dades();
                        tfCodi.setEditable(false);
                        tfCodi.setDisable(true);
                        accio = 2;
                    }
                });
            } else {
                btnDelete.setDisable(true);
            }
        });
    }

    @FXML
    private void btnFirstOnAction(ActionEvent event) {
        Utilitats.nBtnFirst(tvVies);
    }

    @FXML
    private void btnRightOnAction(ActionEvent event) {
        Utilitats.nBtnRight(tvVies);
    }

    @FXML
    private void btnLeftOnAction(ActionEvent event) {
        Utilitats.nBtnLeft(tvVies);
    }

    @FXML
    private void btnLastOnAction(ActionEvent event) {
        Utilitats.nBtnLast(tvVies);
    }

}
