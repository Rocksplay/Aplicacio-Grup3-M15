package grup3.login;

import grup3.Adreces;
import grup3.Paisos;
import grup3.PaisosHelper;
import grup3.Provincies;
import grup3.ProvinciesHelper;
import grup3.Vies;
import grup3.ViesHelper;
import grup3.classes.Utilitats;
import static grup3.classes.Utilitats.nTeclat;
import static grup3.login.FXMLLoginController.connection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class FXMLAltaAdrecesController implements Initializable {

    @FXML
    TableView<Adreces> tvAdreces;
    @FXML
    private TableColumn<Adreces, Integer> colCodi, colCP, colIdClient;
    @FXML
    private TableColumn<Adreces, String> colCentre, colVia, colPoblacio, colProvincia, colPais, colTelefon, colHorari1, colHorari2;
    @FXML
    TextField tfBuscar;
    @FXML
    TextField tfCodi, tfCentre, tfCP, tfPoblacio, tfTelefon, tfHorari1, tfHorari2, tfIdClient;
    @FXML
    ComboBox<Paisos> cbPais;
    @FXML
    ComboBox<Provincies> cbProvincies;
    @FXML
    ComboBox<Vies> cbVies;
    @FXML
    Button btnGuardar, btnCancelar, btnFirst, btnLast, btnRight, btnLeft;
    static int PRIMER = 0;
    static int POSICIO = 0;
    static int contador;
    static int accio = 0;
    ObservableList<Adreces> llistaAdreces = FXCollections.observableArrayList();
    ObservableList<String> list;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //COMBOBOX PAIS
        try {
            String sqlPaisos = "SELECT nom FROM pais";
            PreparedStatement consulta = connection.prepareStatement(sqlPaisos);
            ResultSet resultado = consulta.executeQuery();
            ObservableList<Paisos> llistaPaisos = FXCollections.observableArrayList();
            while (resultado.next()) {
                Paisos pais = new Paisos();
                pais.setNom(resultado.getString(1));
                llistaPaisos.add(pais);
            }
            cbPais.getItems().addAll(llistaPaisos);

        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //COMBOBOX VIES
        try {
            String sqlVies = "SELECT nom FROM via";
            PreparedStatement consulta2 = connection.prepareStatement(sqlVies);
            ResultSet resultado2 = consulta2.executeQuery();
            ObservableList<Vies> llistaVies = FXCollections.observableArrayList();
            while (resultado2.next()) {
                Vies via = new Vies();
                via.setVia(resultado2.getString(1));
                llistaVies.add(via);
            }
            cbVies.getItems().addAll(llistaVies);

        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //COMBOBOX PROVINCIES
        try {
            String sqlProvincies = "SELECT nom FROM provincia";
            PreparedStatement consulta3 = connection.prepareStatement(sqlProvincies);
            ResultSet resultado3 = consulta3.executeQuery();
            ObservableList<Provincies> llistaProvincies = FXCollections.observableArrayList();
            while (resultado3.next()) {
                Provincies provincia = new Provincies();
                provincia.setNom(resultado3.getString(1));
                llistaProvincies.add(provincia);
            }
            cbProvincies.getItems().addAll(llistaProvincies);

        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //
        tfCodi.setDisable(true);
        configuraColumnes();
        refresca();
        nTeclat(tvAdreces);

        selecciona();

        BooleanBinding campsObligatoris = tfCentre.textProperty().isEmpty().or(tfCodi.textProperty().isEmpty());
        btnGuardar.disableProperty().bind(campsObligatoris);
    }

    private void configuraColumnes() {
        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
        colCentre.setCellValueFactory(new PropertyValueFactory<>("centre"));
        colVia.setCellValueFactory(new PropertyValueFactory<>("vies"));
        colCP.setCellValueFactory(new PropertyValueFactory<>("cp"));
        colPoblacio.setCellValueFactory(new PropertyValueFactory<>("poblacio"));
        colProvincia.setCellValueFactory(new PropertyValueFactory<>("provincia"));
        colPais.setCellValueFactory(new PropertyValueFactory<>("pais"));
        colTelefon.setCellValueFactory(new PropertyValueFactory<>("telefon"));
        colHorari1.setCellValueFactory(new PropertyValueFactory<>("horari1"));
        colHorari2.setCellValueFactory(new PropertyValueFactory<>("horari2"));
        colIdClient.setCellValueFactory(new PropertyValueFactory<>("idClient"));
    }

    private ObservableList<Adreces> obtenirDades() {
        contador = 0;
        String sql = "SELECT * FROM adreces";
        ArrayList<Adreces> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Adreces a = new Adreces();
                a.setCodi(resultSet.getInt(1));
                a.setCentre(resultSet.getString(2));

                Vies v = ViesHelper.obteVies(resultSet.getString(3), connection);
                a.setVies(v);

                a.setCp(resultSet.getInt(4));
                a.setPoblacio(resultSet.getString(5));

                Provincies provincia = ProvinciesHelper.obteProvincies(resultSet.getString(6), connection);
                a.setProvincia(provincia);

                Paisos p = PaisosHelper.obtePais(resultSet.getString(7), connection);
                a.setPais(p);

                a.setTelefon(resultSet.getString(8));
                a.setHorari1(resultSet.getString(9));
                a.setHorari2(resultSet.getString(10));
                a.setIdClient(resultSet.getInt(11));
                list.add(a);
                contador++;

            }

        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList<Adreces> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }

    @FXML
    private void btnSortirOnAction(ActionEvent event) {
        Stage altaAdreces = (Stage) btnGuardar.getScene().getWindow();
        altaAdreces.close();
    }

    @FXML
    private void btnNovaAdrecaOnAction(ActionEvent event) {
        accio = 1;
        desacMov();
        tvAdreces.getSelectionModel().clearSelection();
        tvAdreces.setDisable(true);
        buida();
        if (accio == 1) {
            if (contador != 0) {
                contador = contador + 1;
            }
            tfCodi.setText(String.valueOf(contador));
        }
        tfCodi.setText(String.valueOf(contador));
        btnCancelar.setVisible(true);
        buida();
    }

    @FXML
    private void btnGuardarOnAction(ActionEvent event) throws SQLException {

        if (accio == 1) {
            String nou = "INSERT INTO adreces VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
            int codi = Integer.valueOf(tfCodi.getText());
            String centre = tfCentre.getText();
            Vies via = cbVies.getSelectionModel().getSelectedItem();
            int cp = Integer.valueOf(tfCP.getText());
            String poblacio = tfPoblacio.getText();
            Provincies provincia = cbProvincies.getSelectionModel().getSelectedItem();
            Paisos pais = cbPais.getSelectionModel().getSelectedItem();
            String telefon = tfTelefon.getText();
            String horari1 = tfHorari1.getText();
            String horari2 = tfHorari2.getText();
            int id_client = Integer.valueOf(tfIdClient.getText());

            btnCancelar.setVisible(false);
            desacMov();

            preparedStatement2 = connection.prepareStatement(nou);

            preparedStatement2.setInt(1, codi);
            preparedStatement2.setString(2, centre);
            preparedStatement2.setString(3, via.toString());
            preparedStatement2.setInt(4, cp);
            preparedStatement2.setString(5, poblacio);
            preparedStatement2.setString(6, provincia.toString());
            preparedStatement2.setString(7, pais.toString());
            preparedStatement2.setString(8, telefon);
            preparedStatement2.setString(9, horari1);
            preparedStatement2.setString(10, horari2);
            preparedStatement2.setInt(11, id_client);
            preparedStatement2.execute();

            buida();
            refresca();
            accio = 0;
        } else {
            String mod = "UPDATE adreces SET "
                    + "centre = ?, "
                    + "via = ?, "
                    + "cp = ?, "
                    + "poblacio = ?, "
                    + "provincia = ?, "
                    + "pais = ?, "
                    + "telefon = ?, "
                    + "horari1 = ?, "
                    + "horari2 = ?, "
                    + "id_client = ? "
                    + "WHERE codi = ?";
            PreparedStatement update = connection.prepareStatement(mod);
            update.setString(1, tfCentre.getText());
            update.setString(2, cbVies.getSelectionModel().getSelectedItem().toString());
            update.setInt(3, Integer.valueOf(tfCP.getText()));
            update.setString(4, tfPoblacio.getText());
            update.setString(5, cbProvincies.getSelectionModel().getSelectedItem().toString());
            update.setString(6, cbPais.getSelectionModel().getSelectedItem().toString());
            update.setString(7, tfTelefon.getText());
            update.setString(8, tfHorari1.getText());
            update.setString(9, tfHorari2.getText());
            update.setInt(10, Integer.valueOf(tfIdClient.getText()));
            update.setString(11, tfCodi.getText());
            update.executeUpdate();
            refresca();
        }
    }

    @FXML
    private void btnEliminarOnAction(ActionEvent event) {
        try {
            String del = "DELETE FROM adreces WHERE codi = ?";
            PreparedStatement psDel = connection.prepareStatement(del);
            psDel.setString(1, tfCodi.getText());
            psDel.executeUpdate();
            refresca();
            contador = 0;
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void taulaFiltratge() {
        FilteredList<Adreces> filteredData = new FilteredList<>(tvAdreces.getItems(), a -> true);
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(adreces -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                String upperCaseFilter = newValue.toUpperCase();
                if (adreces.getCentre().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;
                }
                return false;
            });
        });
        SortedList<Adreces> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tvAdreces.comparatorProperty());
        tvAdreces.setItems(sortedData);
    }

    private void buida() {
        tfCentre.setText("");
        cbVies.getSelectionModel().clearSelection();
        tfCP.setText("");
        tfPoblacio.setText("");
        cbProvincies.getSelectionModel().clearSelection();
        cbPais.getSelectionModel().clearSelection();
        tfTelefon.setText("");
        tfHorari1.setText("");
        tfHorari2.setText("");
        tfIdClient.setText("");
    }

    @FXML
    private void cancelaAction() {
        buida();
        refresca();
        tvAdreces.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();
    }

    private void selecciona() {
        tvAdreces.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Adreces> observable, Adreces oldValue, Adreces newValue) -> {
            if (newValue != null) {
                tfCodi.setText(String.valueOf(newValue.getCodi()));
                tfCentre.setText(newValue.getCentre());
                tfCP.setText(String.valueOf(newValue.getCp()));
                tfPoblacio.setText(newValue.getPoblacio());
                tfTelefon.setText(newValue.getTelefon());
                tfHorari1.setText(newValue.getHorari1());
                tfHorari2.setText(newValue.getHorari2());
                tfIdClient.setText(String.valueOf(newValue.getIdClient()));
                cbPais.getSelectionModel().select(newValue.getPais());
                cbVies.getSelectionModel().select(newValue.getVies());
                cbProvincies.getSelectionModel().select(newValue.getProvincia());
            }
        });
    }

    private void desacMov() {
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }

    private void activaMov() {
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }

    private void refresca() {
        tvAdreces.setItems(obtenirDades());
        selecciona();
        taulaFiltratge();
    }

    @FXML
    private void btnFirstOnAction(ActionEvent event) {
        Utilitats.nBtnFirst(tvAdreces);
    }

    @FXML
    private void btnRightOnAction(ActionEvent event) {
        Utilitats.nBtnRight(tvAdreces);
    }

    @FXML
    private void btnLeftOnAction(ActionEvent event) {
        Utilitats.nBtnLeft(tvAdreces);
    }

    @FXML
    private void btnLastOnAction(ActionEvent event) {
        Utilitats.nBtnLast(tvAdreces);
    }

}
