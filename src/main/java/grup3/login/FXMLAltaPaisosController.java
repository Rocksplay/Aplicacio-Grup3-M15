//package grup3.login;
//
//import grup3.Paisos;
//import grup3.classes.Utilitats;
//import static grup3.classes.Utilitats.nTeclat;
//import static grup3.login.FXMLLoginController.connection;
//import java.net.URL;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.ResourceBundle;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javafx.beans.binding.BooleanBinding;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.collections.transformation.FilteredList;
//import javafx.collections.transformation.SortedList;
//import javafx.fxml.Initializable;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.scene.control.Button;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.control.cell.PropertyValueFactory;
//import javafx.stage.Stage;
//
//public class FXMLAltaPaisosController implements Initializable {
//
//    @FXML
//    TableView<Paisos> tvPaisos;
//    @FXML
//    private TableColumn<Paisos, Integer> colID;
//    @FXML
//    private TableColumn<Paisos, String> colNom;
//    @FXML
//    private TableColumn<Paisos, String> colCodi;
//    @FXML
//    TextField tfBuscar;
//    @FXML
//    TextField tfID;
//    @FXML
//    TextField tfNom;
//    @FXML
//    TextField tfCodi;
//    @FXML
//    Button btnGuardar;
//    @FXML
//    Button btnNou;
//    @FXML
//    Button btnCancelar;
//    @FXML 
//    Button btnFirst;
//    @FXML 
//    Button btnLeft;
//    @FXML 
//    Button btnRight;
//    @FXML 
//    Button btnLast;
//       
//    static int PRIMER = 0;
//    static int POSICIO = 0;
//    static int contador = 0;
//    ObservableList<Paisos> llistaPaisos = FXCollections.observableArrayList();
//
//    @Override
//    public void initialize(URL url, ResourceBundle rb) {
//            tfID.setDisable(true);
//            configuraColumnes();
//            refresca();
//            nTeclat(tvPaisos);
//            
//            tvPaisos.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Paisos> observable, Paisos oldValue, Paisos newValue) -> {
//            if (newValue != null) {
//                
//                tfID.setText(String.valueOf(newValue.getId()));
//                tfNom.setText(newValue.getNom());
//                tfCodi.setText(String.valueOf(newValue.getCodi()));
//            }
//        });
//        BooleanBinding campsObligatoris = tfNom.textProperty().isEmpty().or(tfCodi.textProperty().isEmpty());
//        btnGuardar.disableProperty().bind(campsObligatoris);
//    }
//
//    private void configuraColumnes() {
//        colID.setCellValueFactory(new PropertyValueFactory<>("id"));
//        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
//        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
//    }
//    
//    private void buida() {
//        tfCodi.setText("");
//        tfID.setText("");
//        tfNom.setText("");
//    }
//    
//    private void desacMov(){
//        btnFirst.setDisable(true);
//        btnRight.setDisable(true);
//        btnLeft.setDisable(true);
//        btnLast.setDisable(true);
//    }
//    
//    private void activaMov(){
//        btnFirst.setDisable(false);
//        btnRight.setDisable(false);
//        btnLeft.setDisable(false);
//        btnLast.setDisable(false);
//    }
//
//    private ObservableList<Paisos> obtenirDades() {
//        String sql = "SELECT id, nom, codi FROM pais";
//        ArrayList<Paisos> list = new ArrayList<>();
//
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        try {
//            preparedStatement = connection.prepareStatement(sql);
//            resultSet = preparedStatement.executeQuery();
//
//            while (resultSet.next()) {
//                Paisos p = new Paisos();
//                p.setId(resultSet.getInt(1));
//                p.setNom(resultSet.getString(2));
//                p.setCodi(resultSet.getString(3));
//                list.add(p);
//                contador++;
//                
//            }
//            
//        } catch (SQLException ex) {
//            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        ObservableList<Paisos> tmp = FXCollections.observableArrayList(list);
//        return tmp;
//    }
//    
//    @FXML
//    private void btnSortirOnAction(ActionEvent event) {
//        Stage altaPaisos = (Stage) btnGuardar.getScene().getWindow();
//        altaPaisos.close();
//        contador = 0;
//    }
//    
//    @FXML
//    private void cancelaAction() {
//        buida();
//        tvPaisos.setDisable(false);
//        btnCancelar.setVisible(false);
//        activaMov();
//    }
//    
//    @FXML
//    private void btnNouPaisOnAction(ActionEvent event) {
//        tvPaisos.getSelectionModel().clearSelection();
//        tvPaisos.setDisable(true);
//        btnCancelar.setVisible(true);
//        desacMov();
//        tfID.setText(String.valueOf(contador + 1));
//        tfNom.setText("");
//        tfCodi.setText("");
//        contador = 0;
//    }
//    
//    @FXML
//    private void btnGuardarOnAction(ActionEvent event) throws SQLException {
//        String nou = "INSERT INTO pais VALUES(?,?,?)";
//        PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
//        int id =  Integer.valueOf(tfID.getText());
//        String nom = tfNom.getText();
//        String codi = tfCodi.getText();
//        
//        preparedStatement2 = connection.prepareStatement(nou);
//        
//        preparedStatement2.setInt(1, id);
//        preparedStatement2.setString(2, nom);
//        preparedStatement2.setString(3, codi);
//        preparedStatement2.execute();
//        
//        tvPaisos.setDisable(false);
//        tfID.setText("");
//        tfNom.setText("");
//        tfCodi.setText("");
//        refresca();
//        contador = 0;
//        btnCancelar.setVisible(false);
//        tvPaisos.setDisable(false);
//        activaMov();
//    }
//    
//    @FXML
//    private void btnEliminarOnAction(ActionEvent event) {
//        try {
//            String del = "DELETE FROM pais WHERE id = ?";
//            PreparedStatement psDel = connection.prepareStatement(del);
//            psDel.setString(1, tfID.getText());
//            psDel.executeUpdate();
//            refresca();
//            contador = 0;
//        } catch (SQLException ex) {
//            Logger.getLogger(FXMLAltaPaisosController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    
//    private void taulaFiltratge() {        
//        FilteredList<Paisos> filteredData = new FilteredList<>(tvPaisos.getItems(), p -> true);
//        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
//            filteredData.setPredicate(paisos -> {
//                if(newValue == null || newValue.isEmpty()) {
//                    return true;
//                }
//                String lowerCaseFilter = newValue.toLowerCase();
//                String upperCaseFilter = newValue.toUpperCase();
//                if(paisos.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
//                    return true;
//                } else if (paisos.getCodi().toUpperCase().indexOf(upperCaseFilter) != -1) {
//                    return true;                 
//                }
//                return false;
//            });
//        });
//        SortedList<Paisos> sortedData = new SortedList<>(filteredData);
//        sortedData.comparatorProperty().bind(tvPaisos.comparatorProperty());
//        tvPaisos.setItems(sortedData);
//    }
//    
//    private void refresca() {                
//        tvPaisos.setItems(obtenirDades());
//        taulaFiltratge();
//    }
//        
//    @FXML
//    private void btnFirstOnAction (ActionEvent event) {
//        Utilitats.nBtnFirst(tvPaisos);
//    }
//    
//    @FXML
//    private void btnRightOnAction (ActionEvent event) {
//        Utilitats.nBtnRight(tvPaisos);
//    }
//    
//    @FXML
//    private void btnLeftOnAction (ActionEvent event) {
//        Utilitats.nBtnLeft(tvPaisos);    
//    }
//    
//    @FXML
//    private void btnLastOnAction (ActionEvent event) {
//        Utilitats.nBtnLast(tvPaisos);
//    }
//}
package grup3.login;

import grup3.Paisos;
import grup3.classes.Utilitats;
import static grup3.classes.Utilitats.nTeclat;
import static grup3.login.FXMLLoginController.connection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class FXMLAltaPaisosController implements Initializable {

    @FXML
    TableView<Paisos> tvPaisos;
    @FXML
    private TableColumn<Paisos, Integer> colID;
    @FXML
    private TableColumn<Paisos, String> colNom;
    @FXML
    private TableColumn<Paisos, String> colCodi;
    @FXML
    TextField tfBuscar;
    @FXML
    TextField tfID;
    @FXML
    TextField tfNom;
    @FXML
    TextField tfCodi;
    @FXML
    Button btnGuardar;
    @FXML
    Button btnNou;
    @FXML
    Button btnCancelar;   
    @FXML 
    Button btnFirst;
    @FXML 
    Button btnLeft;
    @FXML 
    Button btnRight;
    @FXML 
    Button btnLast;
    
    static int PRIMER = 0;
    static int POSICIO = 0;
    static int contador = 0;
    ObservableList<Paisos> llistaPaisos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
            tfID.setDisable(true);
            configuraColumnes();
            refresca();
            nTeclat(tvPaisos);
            
            tvPaisos.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Paisos> observable, Paisos oldValue, Paisos newValue) -> {
            if (newValue != null) {
                
                tfID.setText(String.valueOf(newValue.getId()));
                tfNom.setText(newValue.getNom());
                tfCodi.setText(String.valueOf(newValue.getCodi()));
            }
        });
        BooleanBinding campsObligatoris = tfNom.textProperty().isEmpty().or(tfCodi.textProperty().isEmpty());
        btnGuardar.disableProperty().bind(campsObligatoris);
    }

    private void configuraColumnes() {
        colID.setCellValueFactory(new PropertyValueFactory<>("id"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
    }

    private ObservableList<Paisos> obtenirDades() {
        String sql = "SELECT id, nom, codi FROM pais";
        ArrayList<Paisos> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Paisos p = new Paisos();
                p.setId(resultSet.getInt(1));
                p.setNom(resultSet.getString(2));
                p.setCodi(resultSet.getString(3));
                list.add(p);
                contador++;
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList<Paisos> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }
    @FXML
    private void btnSortirOnAction(ActionEvent event) {
        Stage altaPaisos = (Stage) btnGuardar.getScene().getWindow();
        altaPaisos.close();
        contador = 0;
    }
    @FXML
    private void btnNouPaisOnAction(ActionEvent event) {
        tvPaisos.getSelectionModel().clearSelection();
        tvPaisos.setDisable(true);
        btnCancelar.setVisible(true);
        desacMov();
        tfID.setText(String.valueOf(contador + 1));
        tfNom.setText("");
        tfCodi.setText("");
        contador = 0;
    }
    
    @FXML
    private void btnGuardarOnAction(ActionEvent event) throws SQLException {
        String nou = "INSERT INTO pais VALUES(?,?,?)";
        PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
        int id =  Integer.valueOf(tfID.getText());
        String nom = tfNom.getText();
        String codi = tfCodi.getText();
        
        preparedStatement2 = connection.prepareStatement(nou);
        
        preparedStatement2.setInt(1, id);
        preparedStatement2.setString(2, nom);
        preparedStatement2.setString(3, codi);
        preparedStatement2.execute();
        
        btnCancelar.setVisible(false);
        tvPaisos.setDisable(false);
        tfID.setText("");
        tfNom.setText("");
        tfCodi.setText("");
        refresca();
        contador = 0;
    }
    
    @FXML
    private void btnEliminarOnAction(ActionEvent event) {
        try {
            String del = "DELETE FROM pais WHERE id = ?";
            PreparedStatement psDel = connection.prepareStatement(del);
            psDel.setString(1, tfID.getText());
            psDel.executeUpdate();
            refresca();
            contador = 0;
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaPaisosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void taulaFiltratge() {        
        FilteredList<Paisos> filteredData = new FilteredList<>(tvPaisos.getItems(), p -> true);
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(paisos -> {
                if(newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                String upperCaseFilter = newValue.toUpperCase();
                if(paisos.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else if (paisos.getCodi().toUpperCase().indexOf(upperCaseFilter) != -1) {
                    return true;                 
                }
                return false;
            });
        });
        SortedList<Paisos> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tvPaisos.comparatorProperty());
        tvPaisos.setItems(sortedData);
    }
    
    private void refresca() {                
        tvPaisos.setItems(obtenirDades());
        taulaFiltratge();
    }
    
    private void buida() {
        tfCodi.setText("");
        tfID.setText("");
        tfNom.setText("");
    }
    
    @FXML
    private void cancelaAction() {
        buida();
        tvPaisos.setDisable(false);
        btnCancelar.setVisible(false);
        activaMov();
    }
    
    private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }
        
    @FXML
    private void btnFirstOnAction (ActionEvent event) {
        Utilitats.nBtnFirst(tvPaisos);
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
        Utilitats.nBtnRight(tvPaisos);
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        Utilitats.nBtnLeft(tvPaisos);    
    }
    
    @FXML
    private void btnLastOnAction (ActionEvent event) {
        Utilitats.nBtnLast(tvPaisos);
    }
}
