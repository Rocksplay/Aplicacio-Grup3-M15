package grup3.login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static grup3.login.FXMLLoginController.connection;
import grup3.login.FXMLUsuarisController.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLEditUsuariController implements Initializable {
    int admin;
    int bloqued;
    private String usuari;
    final String UPDATE = "UPDATE login SET "
                                + "usuari = ? ,"
                                + "contrasenya = ? ,"
                                + "bloquejat = ?, "
                                + "nom = ? , "
                                + "cognom = ? , "
                                + "email = ? , "
                                + "carrer = ? , "
                                + "ciutat = ? , "
                                + "telefon = ? , "
                                +"admin = ?"
                         + " WHERE usuari = ?";

    @FXML
    private TextField tFNomU;
    @FXML
    private TextField tFPassU;
    @FXML
    private TextField tFCognomU;
    @FXML
    private TextField tFCarrerU;
    @FXML
    private TextField tFUsuariU;
    @FXML
    private TextField tFCiutatU;
    @FXML
    private TextField tFEmailU;
    @FXML
    private TextField tFTelefonU;
    @FXML
    private CheckBox cBAdminU;
    @FXML
    private CheckBox cBBlokU;
    @FXML
    private Button bTnGuardar;
    public String nomtr;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FXMLUsuarisController Usuaris = new FXMLUsuarisController();
            nomtr = Usuaris.nom;
            String sql = "SELECT * FROM login WHERE usuari = ? ";
            PreparedStatement consulta = connection.prepareStatement(sql);
            consulta.setString(1, Usuaris.nom);

            ResultSet resultado = consulta.executeQuery();

            if (resultado.next()) {
                String usuari = resultado.getString("usuari");
                String contrasenya = resultado.getString("contrasenya");
                int bloquejat = resultado.getInt("bloquejat");
                int admin = resultado.getInt("admin");
                String nom = resultado.getString("nom");
                String cognom = resultado.getString("cognom");
                String email = resultado.getString("email");
                String carrer = resultado.getString("carrer");
                String ciutat = resultado.getString("ciutat");
                String telefon = resultado.getString("telefon");

                tFUsuariU.setText(usuari);
                tFPassU.setText(desencriptar(contrasenya));
                tFNomU.setText(nom);
                tFCognomU.setText(cognom);
                tFEmailU.setText(email);
                tFCarrerU.setText(carrer);
                tFCiutatU.setText(ciutat);
                tFTelefonU.setText(telefon);
                
                if (bloquejat == 1) {
                    cBBlokU.setSelected(true);
                } else {
                    cBBlokU.setSelected(false);
                }

                if (admin == 1) {
                    cBAdminU.setSelected(true);
                } else {
                    cBAdminU.setSelected(false);
                }
            }

        } catch (Exception e) {
        
        }

        //
        tFNomU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFPassU.getText().isEmpty()) {
                    tFPassU.requestFocus();
                } else {

                }
            }
        });
        tFPassU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFCognomU.getText().isEmpty()) {
                    tFCognomU.requestFocus();
                } else {

                }
            }
        });
        tFCognomU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFCarrerU.getText().isEmpty()) {
                    tFCarrerU.requestFocus();
                } else {

                }
            }
        });
        tFCarrerU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFUsuariU.getText().isEmpty()) {
                    tFUsuariU.requestFocus();
                } else {

                }
            }
        });
        tFUsuariU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFCiutatU.getText().isEmpty()) {
                    tFCiutatU.requestFocus();
                } else {

                }
            }
        });
        tFCiutatU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFEmailU.getText().isEmpty()) {
                    tFEmailU.requestFocus();
                } else {

                }
            }
        });
        tFEmailU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFTelefonU.getText().isEmpty()) {
                    tFTelefonU.requestFocus();
                } else {

                }
            }
        });
        tFTelefonU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                cBAdminU.requestFocus();
            }
        });
        cBAdminU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                cBBlokU.requestFocus();
            }
        });
        cBBlokU.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                bTnGuardar.requestFocus();
            }
        });

    }
    
    public void setUsuari (String usuari) {
       this.usuari = usuari;
   }

    private static String desencriptar(String s) throws UnsupportedEncodingException {
        byte[] decode = Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    }

    private static String encriptar(String s) throws UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }
    
    @FXML
    private void bTnGuardarAction (ActionEvent event ) throws SQLException, UnsupportedEncodingException {
        
            if (cBBlokU.isSelected() == true){
                 bloqued = 1;
            }
            else{
                 bloqued = 0;
            }
            
            if (cBAdminU.isSelected() == true){
                 admin = 1;
            }
            else{
                 admin = 0;
            }
            PreparedStatement update = connection.prepareStatement(UPDATE);
            update.setString(1, tFUsuariU.getText());
            update.setString(2, encriptar(tFPassU.getText()));
            update.setInt(3, bloqued);
            update.setString(4, tFNomU.getText());
            update.setString(5, tFCognomU.getText());
            update.setString(6, tFEmailU.getText());
            update.setString(7, tFCarrerU.getText());
            update.setString(8, tFCiutatU.getText());
            update.setString(9, tFTelefonU.getText());
            update.setInt(10, admin);
            update.setString(11, nomtr);
            update.executeUpdate();
  
            Stage stageUsu = (Stage) tFTelefonU.getScene().getWindow();
            stageUsu.close();
            
    }
    @FXML
    private void cambiarpassAction(ActionEvent event) throws IOException{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLchangePswU.fxml"));
            Parent root = (Parent) loader.load();
            FXMLchangePSWU controller = loader.getController();
            controller.setUsuari(nomtr);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.setScene(new Scene(root));
            stage.show();
    
    }
}

