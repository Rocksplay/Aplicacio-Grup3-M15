/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3.login;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;


public class FXMLinfoController implements Initializable {

    @FXML
    private Text txtJordi;
    @FXML
    private ImageView imgJordi;
    
    @FXML
    private ImageView imgJoan;
    
    @FXML
    private ImageView imgAlex;
        
    @FXML
    private ImageView imgRoger;
    
    @FXML
    private void actionJordi(ActionEvent event) throws IOException {
        imgJoan.setVisible(false);
        imgAlex.setVisible(false);
        imgRoger.setVisible(false);
        if(imgJordi.isVisible()){
            imgJordi.setVisible(false);
        }
        else{
            imgJordi.setVisible(true);
        }
    }
    
    @FXML
    private void actionJoan(ActionEvent event) throws IOException {
        imgJordi.setVisible(false);
        imgAlex.setVisible(false);
        imgRoger.setVisible(false);
        if(imgJoan.isVisible()){
            imgJoan.setVisible(false);
        }
        else{
            imgJoan.setVisible(true);
        }
    }
    
    @FXML
    private void actionAlex(ActionEvent event) throws IOException {
        imgJordi.setVisible(false);
        imgJoan.setVisible(false);
        imgRoger.setVisible(false);
        if(imgAlex.isVisible()){
            imgAlex.setVisible(false);
        }
        else{
            imgAlex.setVisible(true);
        }
    }
    
    @FXML
    private void actionRoger(ActionEvent event) throws IOException {
        imgJordi.setVisible(false);
        imgAlex.setVisible(false);
        imgJoan.setVisible(false);
        if(imgRoger.isVisible()){
            imgRoger.setVisible(false);
        }
        else{
            imgRoger.setVisible(true);
        }
    }
        

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }    
    
}
