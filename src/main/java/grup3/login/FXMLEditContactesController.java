package grup3.login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static grup3.login.FXMLLoginController.connection;
import grup3.login.FXMLContacteController.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import static java.util.Collections.list;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLEditContactesController implements Initializable {
    int admin;
    int bloqued;
    private String usuari;
    final String UPDATE = "UPDATE contactes SET "
                                + "nom = ? ,"
                                + "carrec = ? ,"
                                + "departament = ?, "
                                + "telefon1 = ? , "
                                + "telefon2 = ? , "
                                + "fax = ? , "
                                + "correu = ? , "
                                + "horari1 = ? , "
                                + "horari2 = ? , "
                                + "contacte_principal = ? , "
                                +"id_parent = ?"
                         + " WHERE nom = ?";

    @FXML
    private TextField tFNom;
    @FXML
    private TextField tFHorariE;
    @FXML
    private TextField tFHorariS;
    @FXML
    private TextField tFEmail;
    @FXML
    private TextField tFFax;
    @FXML
    private TextField tFTelefon1;
    @FXML
    private TextField tFTelefon2;
    @FXML
    private CheckBox cBContactePrincipal;
    @FXML
    private ComboBox cBCarrec;  
    @FXML
    private ComboBox cBDepartament;
    @FXML
    private ComboBox cBClient;
    @FXML
    private Button bTnGuardar;
    public String nomtr;
    FXMLContacteController Contactes = new FXMLContacteController();
   
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            nomtr = Contactes.nom;
            String sql = "SELECT * FROM contactes WHERE nom = ? ";
            PreparedStatement consulta = connection.prepareStatement(sql);
            consulta.setString(1, Contactes.nom);

            ResultSet resultado = consulta.executeQuery();
            
            String sql_carrec = "SELECT * FROM carrec ";
            PreparedStatement consulta2 = connection.prepareStatement(sql_carrec);
            ResultSet resultado2 = consulta2.executeQuery();
            
            String sql_departament= "SELECT * FROM departament ";
            PreparedStatement consulta4 = connection.prepareStatement(sql_departament);
            ResultSet resultado4= consulta4.executeQuery();

            
            String sql_client= "SELECT * FROM clients ";
            PreparedStatement consulta5 = connection.prepareStatement(sql_client);
            ResultSet resultado5= consulta5.executeQuery();

            if (resultado.next()) {
                String nom = resultado.getString("nom");  
                int carrec = resultado.getInt("carrec");
                int departament = resultado.getInt("departament");
                String telefon1 = resultado.getString("telefon1");
                String telefon2 = resultado.getString("telefon2");
                String fax = resultado.getString("fax");
                String correu = resultado.getString("correu");
                String horari1 = resultado.getString("horari1");
                String horari2 = resultado.getString("horari2");                
                int contacte_principal = resultado.getInt("contacte_principal");
                int id_parent = resultado.getInt("id_parent");
                
                  /////////////////////////////////////
                  //COMBOBOX CARREC
                  /////////////////////////////////////
                  while (resultado2.next()) {
                  String carrecs = resultado2.getString("nom");
                  ObservableList<String> list = FXCollections.observableArrayList(carrecs);
                  cBCarrec.getItems().addAll(list);
                  
                  String sql3 = "SELECT nom FROM carrec WHERE codi = ? ";
                  PreparedStatement consulta3 = connection.prepareStatement(sql3);
                  consulta3.setInt(1, carrec);
                  ResultSet resultado3 = consulta3.executeQuery();
                  while (resultado3.next()) {
                           String nom2 = resultado3.getString("nom");    
                           cBCarrec.setValue(nom2);
                  }}
                  
                  ///////////////////////////////////
                  
                  
                  /////////////////////////////////////
                  //COMBOBOX DEPARTAMENT
                  /////////////////////////////////////
                  while (resultado4.next()) {
                  String departaments = resultado4.getString("nom");
                  ObservableList<String> list = FXCollections.observableArrayList(departaments);
                  cBDepartament.getItems().addAll(list);
                  
                  String sql3 = "SELECT nom FROM departament WHERE codi = ? ";
                  PreparedStatement consulta3 = connection.prepareStatement(sql3);
                  consulta3.setInt(1, departament);
                  ResultSet resultado3 = consulta3.executeQuery();
                  while (resultado3.next()) {
                           String nom2 = resultado3.getString("nom");    
                           cBDepartament.setValue(nom2);
                  }}
                 
                  ///////////////////////////////////
                  
                  
                  /////////////////////////////////////
                  //COMBOBOX CLIENT
                  /////////////////////////////////////
                  while (resultado5.next()) {
                  String clients = resultado5.getString("nom_comercial");
                  ObservableList<String> list = FXCollections.observableArrayList(clients);
                  cBClient.getItems().addAll(list);
                  
                  String sql3 = "SELECT nom_comercial FROM clients WHERE id_referencia = ? ";
                  PreparedStatement consulta3 = connection.prepareStatement(sql3);
                  consulta3.setInt(1, id_parent);
                  ResultSet resultado3 = consulta3.executeQuery();
                  while (resultado3.next()) {
                           String nom2 = resultado3.getString("nom_comercial");    
                           cBClient.setValue(nom2);
                  }}
                 
                  ///////////////////////////////////
                  
          
                tFNom.setText(nom);
                tFHorariE.setText(horari1);
                tFHorariS.setText(horari2);                
                tFEmail.setText(correu);
                tFFax.setText(fax);
                tFTelefon1.setText(telefon1);
                tFTelefon2.setText(telefon2);

                if (contacte_principal == 1) {
                    cBContactePrincipal.setSelected(true);
                } else {
                    cBContactePrincipal.setSelected(false);
                }
            }
            

        } catch (Exception e) {
        
        }

    }
    
    public void setUsuari (String usuari) {
       this.usuari = usuari;
   }

    private static String desencriptar(String s) throws UnsupportedEncodingException {
        byte[] decode = Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    }

    private static String encriptar(String s) throws UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }
    
    
    
    @FXML
    private void bTnGuardarAction (ActionEvent event ) throws SQLException, UnsupportedEncodingException {

        
         // COMBOBOX CARREC
         String carrec=cBCarrec.getSelectionModel().getSelectedItem().toString();
         String sql_carrec = "SELECT codi FROM carrec WHERE nom = ? ";
         PreparedStatement consulta = connection.prepareStatement(sql_carrec);
         consulta.setString(1, carrec);
         ResultSet resultado = consulta.executeQuery();
         //////////////////////////////////////
         
         // COMBOBOX DEPARTAMENT
         String departament=cBDepartament.getSelectionModel().getSelectedItem().toString();
         String sql_departament = "SELECT codi FROM departament WHERE nom = ? ";
         PreparedStatement consulta2 = connection.prepareStatement(sql_departament);
         consulta2.setString(1, departament);
         ResultSet resultado2 = consulta2.executeQuery();
         //////////////////////////////////////
         
         // COMBOBOX CLIENT
         String client=cBClient.getSelectionModel().getSelectedItem().toString();
         String sql_client = "SELECT id_referencia FROM clients WHERE nom_comercial = ? ";
         PreparedStatement consulta3 = connection.prepareStatement(sql_client);
         consulta3.setString(1, client);
         ResultSet resultado3 = consulta3.executeQuery();
         //////////////////////////////////////
         
         
            
            if (cBContactePrincipal.isSelected() == true){
                 admin = 1;
            }
            else{
                 admin = 0;
            }
            PreparedStatement update = connection.prepareStatement(UPDATE);
            update.setString(1, tFNom.getText());
            while (resultado.next()) {
                  int codi_carrec = resultado.getInt("codi");
                  update.setInt(2, codi_carrec);
                 }
            
            while (resultado2.next()) {
                  int codi_departament = resultado2.getInt("codi");
                  update.setInt(3, codi_departament);
                 }
            
            update.setString(4, tFTelefon1.getText());
            update.setString(5, tFTelefon2.getText());
            update.setString(6, tFFax.getText());
            update.setString(7, tFEmail.getText());
            update.setString(8, tFHorariE.getText());
            update.setString(9, tFHorariS.getText());
            update.setInt(10, admin);
            
            while (resultado3.next()) {
                  int codi_client = resultado3.getInt("id_referencia");
                  update.setInt(11, codi_client);
                 }
            
            update.setString(12, nomtr);
            update.executeUpdate();
  
            Stage stageUsu = (Stage) tFFax.getScene().getWindow();
            stageUsu.close();
            
            
      

    }

}

