package grup3.login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import grup3.Departaments;
import grup3.Vies;
import grup3.classes.Utilitats;
import static grup3.login.FXMLLoginController.connection;
import static grup3.login.FXMLUsuarisController.POSICIO;
import static grup3.login.FXMLUsuarisController.nom;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

public class FXMLDepartamentsController implements Initializable {
      
    String UPDATE = "UPDATE departament SET "
                                + "nom = ?  "
                                + "WHERE nom = ?";
    
    String INSERT = "INSERT INTO departament VALUES(?,?)";
    
    ObservableList <Departaments> llistaDepartaments = FXCollections.observableArrayList();
    
    Departaments d = new Departaments();
    
    int accio = 0;

    int contador;
    
    String code;
    
    String nom_departament;
    
   @FXML
    private TableColumn<Departaments, Integer> codi;
   
    @FXML
    private TableColumn<Departaments, String> Tipus;
    
    @FXML
    private TableView<Departaments> tvDepartaments;
      
    @FXML
    private Button btnDelete;
    
    @FXML
    private Button btnGuardar;
    
    @FXML
    TableColumn colCodi;
    
    @FXML
    TableColumn colTipus;
    
    @FXML
    TextField tfCodi;
    
    @FXML
    TextField tfDepartament;
    
    @FXML Button btnCancelar;
    
    @FXML 
    Button btnFirst;
    @FXML 
    Button btnLeft;
    @FXML 
    Button btnRight;
    @FXML 
    Button btnLast;
    
    @FXML
    private void guardarOnAction() throws SQLException {
            if(accio==1){
            //ALERT
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Guardar i Sortir");
            alert.setHeaderText(null);
            alert.setContentText("Dades Guardades Correctament");
            alert.showAndWait();

            // CONTADOR FILES PER CODI                
            code=tfCodi.getText(); 
            nom_departament=tfDepartament.getText();
            
            PreparedStatement con1 = connection.prepareStatement(INSERT);
            con1.setInt(1, contador + 1);
            con1.setString(2, nom_departament);
            
            con1.execute();
           
            }
            else{
                modificar();
            }
            
            refresca();
            activaMov();
            
            tfCodi.setEditable(true);            
            btnCancelar.setVisible(false);
            tvDepartaments.setDisable(false);
            
            tfCodi.setText("");
            tfDepartament.setText("");
    }
    
    @FXML
    private void sortirOnAction(ActionEvent event) throws SQLException {
        Stage stageAdmin = (Stage) btnGuardar.getScene().getWindow();
        stageAdmin.close();
    }
    
    @FXML
    private void deleteOnAction(ActionEvent event) throws SQLException {
        FXMLLoginController login = new FXMLLoginController();
        d = tvDepartaments.getItems().get(tvDepartaments.getSelectionModel().getFocusedIndex());
        
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmar eliminar");
            alert.setHeaderText("Eliminar");
            alert.setContentText("Vols eliminar el Departament: " + d.getnomDepartament()+ " ?");

            ButtonType btnSi = new ButtonType("Si");
            ButtonType btnNo = new ButtonType("No");tvDepartaments.getItems().get(tvDepartaments.getSelectionModel().getFocusedIndex());

            alert.getButtonTypes().setAll(btnSi, btnNo);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == btnSi) {
                eliminarDepartament(d.getnomDepartament());
                refresca();
            }
    }
    
    @FXML
    private void cancelaAction() {
        tvDepartaments.setDisable(false);
        tfCodi.setText("");
        tfDepartament.setText("");
        btnCancelar.setVisible(false);
        activaMov();

    }
     
    @FXML
    private void nouDepartamentOnAction(ActionEvent event) throws SQLException {
        accio = 1;
        
        tfCodi.setDisable(true);
        tvDepartaments.setDisable(true);
        btnCancelar.setVisible(true);
        desacMov();
        
        tfCodi.setText("");
        tfDepartament.setText("");
        
         // CONTADOR FILES PER CODI                
         String select_files = "SELECT * FROM departament";
         PreparedStatement con2 = connection.prepareStatement(select_files);
         ResultSet resultado1 = con2.executeQuery();

        while (resultado1.next()) {
            contador = resultado1.getInt("codi");
        }
            
        tfCodi.setText(String.valueOf(contador+1));
        
        tfDepartament.requestFocus();
        
        tfDepartament.setOnKeyPressed(evento -> {
            
            if (evento.getCode().equals(KeyCode.ENTER)){
            
                btnGuardar.requestFocus();
               
            }
        });
         
        btnGuardar.setOnKeyPressed(evento -> {
            if (evento.getCode().equals(KeyCode.ENTER) || evento.getCode().equals(KeyCode.TAB)) {
                 try {
                    guardarOnAction();
                } catch (Exception ex) {
                    Logger.getLogger(FXMLCrearUsuariController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }); 
    }
        
    private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }
   
    private void mostrar_dades(){
        try {
            String sql = "SELECT * FROM departament WHERE nom = ? ";
            PreparedStatement consulta = connection.prepareStatement(sql);
            consulta.setString(1, nom);

            ResultSet resultado = consulta.executeQuery();

            if (resultado.next()) {
                int codi = resultado.getInt("codi");
                String vies = resultado.getString("nom");

                tfCodi.setText(String.valueOf(codi));
                tfDepartament.setText(vies);
                
                nom_departament=tfDepartament.getText();
            }

        } catch (Exception e) {
        
        }
        
    }
     
    private void modificar(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Confirmació de modificació");
        alert.setHeaderText(null);
        alert.setContentText("Dades modificades correctament!");
        alert.showAndWait();
    
        PreparedStatement preparedStatement;
        try {
            System.out.println(nom_departament);
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1,tfDepartament.getText());
            preparedStatement.setString(2,nom_departament);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut modificar el Departament!", ex);            
        }  
    }
    
    private void eliminarDepartament(String Via) {                
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM departament WHERE nom = ?");
            preparedStatement.setString(1,Via);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut eliminar el Departament!", ex);            
        }        
    }
    
    private ObservableList<Departaments> obtenirDades() {        
        String sql = "SELECT codi, nom FROM departament";
        ArrayList<Departaments> list = new ArrayList<>();        

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Departaments v = new Departaments();
                v.setCodi(resultSet.getInt(1));
                v.setTipusVia(resultSet.getString(2));
                list.add(v);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ObservableList<Departaments> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }
    
    private void refresca() {
        llistaDepartaments = obtenirDades();
        tvDepartaments.setItems(llistaDepartaments);
        
        //taulaFiltratge();
    }
    
    private void configuraColumnes(){
    
            colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
            colTipus.setCellValueFactory(new PropertyValueFactory<>("nom"));   
    }
    
    @FXML
    private void btnFirstOnAction (ActionEvent event) {
        Utilitats.nBtnFirst(tvDepartaments);
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
        Utilitats.nBtnRight(tvDepartaments);
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        Utilitats.nBtnLeft(tvDepartaments);    
    }
    
    @FXML
    private void btnLastOnAction (ActionEvent event) {
        Utilitats.nBtnLast(tvDepartaments);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configuraColumnes();
        refresca();
        
        //Doble click
        tvDepartaments.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                btnDelete.setDisable(false);
                nom = newValue.getnomDepartament();
                tvDepartaments.setOnMousePressed(event -> {
                    if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                        mostrar_dades();
                        tfCodi.setEditable(false);
                        tfCodi.setDisable(true);
                        accio=2;
                    }
                });
            } else {
                btnDelete.setDisable(true);
            }
        });
    }    
}    
    
