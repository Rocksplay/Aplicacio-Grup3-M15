package grup3.login;

import grup3.ConnectionUtil;
import static grup3.login.FXMLLoginController.passw;
import static grup3.login.FXMLchangePSW.mes_servidor_nou;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class FXMLCrearUsuariController implements Initializable {

    @FXML
    private Button btnGuardar;
    @FXML
    private TextField tFNom;
    @FXML
    private TextField tFPass;
    @FXML
    private TextField tFReppass;
    @FXML
    private TextField tFUsuari;
    @FXML
    private TextField tFEmail;
    @FXML
    private TextField tFCognom;
    @FXML
    private TextField tFCiutat;
    @FXML
    private TextField tFCarrer;
    @FXML
    private TextField tFTelef;

    @FXML
    private CheckBox check_admin;
    
    @FXML
    private void onActionSortir(ActionEvent event){
            Stage stageAdmin = (Stage) btnGuardar.getScene().getWindow();
            stageAdmin.close();
    }

    int contador;
    int control_admin = 0;

    static String nova_data;
    static String mes_servidor_nou;
    int mesos;

    //VARIABLES
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    public FXMLCrearUsuariController() {
        connection = ConnectionUtil.connectdb();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tFNom.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFCognom.getText().isEmpty()) {
                    tFCognom.requestFocus();
                } else {

                }
            }
        });
        tFCognom.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFTelef.getText().isEmpty()) {
                    tFTelef.requestFocus();
                } else {

                }
            }
        });
        tFTelef.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFCiutat.getText().isEmpty()) {
                    tFCiutat.requestFocus();
                } else {

                }
            }
        });
        tFCiutat.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFCarrer.getText().isEmpty()) {
                    tFCarrer.requestFocus();
                } else {

                }
            }
        });
        tFCarrer.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFUsuari.getText().isEmpty()) {
                    tFUsuari.requestFocus();
                } else {

                }
            }
        });
        tFUsuari.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFEmail.getText().isEmpty()) {
                    tFEmail.requestFocus();
                } else {

                }
            }
        });
        tFEmail.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFPass.getText().isEmpty()) {
                    tFPass.requestFocus();
                } else {

                }
            }
        });
        tFPass.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                if (tFReppass.getText().isEmpty()) {
                    tFReppass.requestFocus();
                } else {

                }
            }
        });
        tFReppass.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                check_admin.requestFocus();
            }
        });

        check_admin.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                btnGuardar.requestFocus();

            }
        });
        check_admin.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)) {
                btnGuardar.requestFocus();

            }
        });
        btnGuardar.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                try {
                    onActionGuardar(null);
                } catch (Exception ex) {
                    Logger.getLogger(FXMLCrearUsuariController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        //Binding camps obligatoris
        BooleanBinding campsObligatoris = tFNom.textProperty().isEmpty().
                or(tFCognom.textProperty().isEmpty()).
                or(tFEmail.textProperty().isEmpty()).
                or(tFUsuari.textProperty().isEmpty()).
                or(tFPass.textProperty().isEmpty()).
                or(tFReppass.textProperty().isEmpty()).
                or(tFCarrer.textProperty().isEmpty()).
                or(tFTelef.textProperty().isEmpty());
        btnGuardar.disableProperty().bind(campsObligatoris);

    }

    //PROGRAMA PER ENCRIPTAR
    private static String encriptar(String s) throws UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }

    @FXML
    public void onActionGuardar(ActionEvent event) throws Exception {

        //COMPROBAR SI LES CONTRASENYES COINCIDEIXEN
        if (tFReppass.getText().equals(tFPass.getText())) {
            //ALERT
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Guardar i Sortir");
            alert.setHeaderText(null);
            alert.setContentText("Dades Guardades Correctament");
            alert.showAndWait();

            // CONTADOR FILES PER CODI                
            String select_files = "SELECT * FROM login";
            PreparedStatement con2 = connection.prepareStatement(select_files);
            ResultSet resultado1 = con2.executeQuery();

            while (resultado1.next()) {
                contador++;
            }

            // USUARI ES ADMINISTRADOR?
            if (check_admin.isSelected() == true) {
                control_admin = 1;
            }

            //MESOS DE BLOQUEIG DE LA BASE DE DADES   
            String query6 = "select * from admin where codi = ?";
            PreparedStatement consulta6 = connection.prepareStatement(query6);
            consulta6.setInt(1, 1);
            ResultSet resultado6 = consulta6.executeQuery();
            while (resultado6.next()) {
                int intents_mesos = resultado6.getInt("caducitat_contrasenya");
                mesos = intents_mesos;
            }

            // CREACIÓ DE LA NOVA DATA DE BLOQUEIG
            SimpleDateFormat yyyy = new SimpleDateFormat("yyyy");
            Date any = new Date();
            String any_servidor2 = yyyy.format(any);
            int any_servidor = Integer.parseInt(any_servidor2);

            SimpleDateFormat MM = new SimpleDateFormat("MM");
            Date mes = new Date();
            String mes_servidor2 = MM.format(mes);
            int mes_servidor = Integer.parseInt(mes_servidor2);

            SimpleDateFormat dd = new SimpleDateFormat("dd");
            Date dia = new Date();
            String dia_servidor = dd.format(dia);

            if (mes_servidor + mesos > 12) {
                mes_servidor = (mes_servidor + mesos) - 12;
                any_servidor = any_servidor + 1;

                if (mes_servidor < 10) {
                    String mes_servidor3 = String.valueOf(mes_servidor);
                    mes_servidor_nou = ("0" + mes_servidor3);
                }

                nova_data = (any_servidor + "-" + mes_servidor_nou + "-" + dia_servidor);

            } else {

                mes_servidor = mes_servidor + 6;
                nova_data = (any_servidor + "-" + mes_servidor + "-" + dia_servidor);

            }

//                //INSERTAR USUARI A LA BBD  
            String usuari_nou = tFUsuari.getText();
            String contrasenya = tFPass.getText();
            String nom = tFNom.getText();
            String cognom = tFCognom.getText();
            String email = tFEmail.getText();
            String carrer = tFCarrer.getText();
            String ciutat = tFCiutat.getText();
            String telefon = tFTelef.getText();

            String cadenaEncriptada = encriptar(contrasenya);

            String nouusu = "INSERT INTO login VALUES (?,?,?,0,?,?,?,?,?,?,?,?)";
            PreparedStatement con1 = connection.prepareStatement(nouusu);
            con1.setInt(1, contador + 1);
            con1.setString(2, usuari_nou);
            con1.setString(3, cadenaEncriptada);
            con1.setString(4, nova_data);
            con1.setString(5, nom);
            con1.setString(6, cognom);
            con1.setString(7, email);
            con1.setString(8, carrer);
            con1.setString(9, ciutat);
            con1.setString(10, telefon);
            con1.setInt(11, control_admin);
            con1.execute();

            //SORTIR A LA PAGINA ANTERIOR
            Stage usuariss = (Stage) tFNom.getScene().getWindow();
            usuariss.close();

        } else if (!tFReppass.getText().equals(tFPass.getText())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error al guardar les dades");
            alert.setHeaderText("Error al guardar les dades");
            alert.setContentText("Les contrasenyes no coincideixen");
            alert.showAndWait();
            tFReppass.setText(" ");
            tFReppass.requestFocus();

        }
    }
        
    @FXML
    protected void intentsTY(KeyEvent event) {
        if(!event.getCharacter().matches("[0-9]")) {
            event.consume();
        }
    }
}

