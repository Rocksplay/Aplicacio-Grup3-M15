package grup3.login;    //codi,nom

import grup3.FormaPagament;
import grup3.classes.Utilitats;
import static grup3.login.FXMLLoginController.connection;
import grup3.operacio;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author m15
 */
public class FXMLtipus_operacio implements Initializable {

    @FXML
    private TableColumn<operacio, Integer> colCodi;
    @FXML
    private TableColumn<operacio, String> colNom;
    @FXML
    private TableView<operacio> tvForma;

    @FXML
    private TextField tFcodi;
    
    @FXML
    private TextField tFnom;
    
    @FXML
    private Button btnRefresh;
   
    @FXML
    private Button btnNew;
    
    @FXML
    private Button btnDelete;
    
    @FXML
    private Button btnCancelar;
    
    @FXML
    private Button btnFirst;
    
    @FXML 
    private  Button btnLeft;
    
    @FXML 
    private Button btnRight;
    
    @FXML 
    private Button btnLast;
    
    public static String nom;
    static int contador=0;
    static boolean insert = true;
    ObservableList<operacio> llistaForma = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configuraColumnes();
        refresca();
        tFcodi.setDisable(true);
        tvForma.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends operacio> observable, operacio oldValue, operacio newValue) -> {
            if (newValue != null) {
                insert = false;
                tFcodi.setText(String.valueOf(newValue.getCodi()));
                tFnom.setText(newValue.getNom());

            } else {
                insert = true;
            }
        });

    }

    private ObservableList<operacio> obtenirDades() {
        contador = 0;
        String sql = "SELECT codi, nom FROM tipus_operacio";
        ArrayList<operacio> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                operacio c = new operacio();
                c.setCodi(resultSet.getInt(1));
                c.setNom(resultSet.getString(2));

                list.add(c);
                contador++;

            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ObservableList<operacio> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }

    private void configuraColumnes() {
        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
    }
    
    @FXML
    private void btnSortirOnAction(ActionEvent event) throws SQLException {
        Stage stageAdmin = (Stage) btnNew.getScene().getWindow();
        stageAdmin.close();
    }

    @FXML
    private void refresca() {
        tvForma.setItems(obtenirDades());
    }

    @FXML
    private void newContacte() {
        refresca();
        buida();
        tFcodi.setText(String.valueOf(contador));
        tvForma.setDisable(true);
        btnCancelar.setVisible(true);
        desacMov();
        
          if (insert) {
            if (contador != 0) contador = contador +1;
          }
           tFcodi.setText(String.valueOf(contador));

    }

    private void buida() {
        tFcodi.setText("");
        tFnom.setText("");
    }
    
    private void desacMov(){
        btnFirst.setDisable(true);
        btnRight.setDisable(true);
        btnLeft.setDisable(true);
        btnLast.setDisable(true);
    }
    
    private void activaMov(){
        btnFirst.setDisable(false);
        btnRight.setDisable(false);
        btnLeft.setDisable(false);
        btnLast.setDisable(false);
    }

    @FXML
    private void guardarAction() throws SQLException {
        refresca();
        configuraColumnes();
        if (insert) {
           
            String nou = "INSERT INTO  tipus_operacio VALUES (?,?)";
            PreparedStatement preparedStatement2 = connection.prepareStatement(nou);
            preparedStatement2.setInt(1, Integer.valueOf(tFcodi.getText()));
            preparedStatement2.setString(2, tFnom.getText());
            preparedStatement2.execute();
             refresca();

        } else {
            System.out.println("updatejant");
           String  update = "UPDATE tipus_operacio SET nom=? WHERE codi = ?";
            PreparedStatement preparedStatement2 = connection.prepareStatement(update);
            preparedStatement2.setString(1, tFnom.getText());
            preparedStatement2.setInt(2, Integer.valueOf(tFcodi.getText()));                        
            preparedStatement2.executeUpdate();
             refresca();
        }

        buida();
        tvForma.setDisable(false);
        btnCancelar.setVisible(true);
        activaMov();
    }

    @FXML
    private void cancelaAction() {
        buida();
        btnCancelar.setVisible(false);
        tvForma.setDisable(false);
        activaMov();
    }

    @FXML
    private void deleteAction() throws SQLException {
        String delete = "DELETE FROM tipus_operacio WHERE codi = ?";
        PreparedStatement preparedStatement3 = connection.prepareStatement(delete);
        preparedStatement3.setString(1, String.valueOf(tFcodi.getText()));
        preparedStatement3.executeUpdate();
        buida();
        refresca();

    }
    @FXML
    private void btnFirstOnAction (ActionEvent event) {
        Utilitats.nBtnFirst(tvForma);
    }
    
    @FXML
    private void btnRightOnAction (ActionEvent event) {
        Utilitats.nBtnRight(tvForma);
    }
    
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        Utilitats.nBtnLeft(tvForma);    
    }
    
    @FXML
    private void btnLastOnAction (ActionEvent event) {
        Utilitats.nBtnLast(tvForma);
    }

}
