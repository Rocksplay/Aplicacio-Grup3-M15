package grup3.login;

import grup3.Contactes;
import grup3.Usuaris;
import grup3.classes.Utilitats;
import static grup3.classes.Utilitats.alerta_warn;
import static grup3.login.FXMLLoginController.connection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author m15
 */
public class FXMLContacteController implements Initializable {

    static int PRIMER = 0;
    static int POSICIO = 0;
    @FXML
    private TableColumn<Contactes, Integer> colCodi;
    @FXML
    private TableColumn<Contactes, String> colNom;
    @FXML
    private TableColumn<Contactes, String> colCarrec;
    @FXML
    private TableColumn<Contactes, String> colDepartament;
    @FXML
    private TableColumn<Contactes, String> colTelefon;
    @FXML
    private TableColumn<Contactes, String> colTelefon2;
    @FXML
    private TableColumn<Contactes, String> colFax;
    @FXML
    private TableColumn<Contactes, String> colEmail;
    @FXML
    private TableColumn<Contactes, String> colContacte;
    @FXML
    private TableColumn<Contactes, String> colHorari1;
    @FXML
    private TableColumn<Contactes, String> colHorari2;
    @FXML
    private TableColumn<Contactes, String> colIdparent;
    @FXML
    TableView<Contactes> tvContactes;
    @FXML
    private TextField tfBuscar;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button btnFirst;
    @FXML
    private Button btnDelete;
    public static String nom;
    ObservableList<Contactes> llistaContactes;
    Contactes c2 = new Contactes();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configuraColumnes();
        refresca();

        tvContactes.getSelectionModel().select(PRIMER);
        //tvUsuaris.getFocusModel().focuUsuariss(PRIMER);

        //Navegacio amb el teclat    
        tvContactes.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.LEFT)) {
                if (tvContactes.getFocusModel().getFocusedIndex() != PRIMER) {
                    tvContactes.requestFocus();
                    tvContactes.getSelectionModel().select(PRIMER);
                    tvContactes.getFocusModel().focus(PRIMER);
                }
            }
            if (event.getCode().equals(KeyCode.RIGHT)) {
                int TOTAL = tvContactes.getItems().size() - 1;
                if (tvContactes.getFocusModel().getFocusedIndex() != TOTAL) {
                    tvContactes.requestFocus();
                    tvContactes.getSelectionModel().select(TOTAL);
                    tvContactes.getFocusModel().focus(TOTAL);
                }
            }
        });

        //Doble click
        tvContactes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                btnDelete.setDisable(false);
                nom = newValue.getNom();
                tvContactes.setOnMousePressed(event -> {
                    if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLEditContactes.fxml"));
                            Parent root = (Parent) loader.load();
                            Stage stage = new Stage();
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.initStyle(StageStyle.DECORATED);
                            stage.setScene(new Scene(root));
                            stage.show();

                            //RNB. S'executa al tancar-se la finestra FXMLCrearUsuari.fxml
                            stage.setOnHidden((WindowEvent event1) -> {
                                refresca();
                            });

                        } catch (IOException ex) {
                            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            } else {
                btnDelete.setDisable(true);
            }
        });

//        llistaContactes = obtenirDades();
//        tvContactes.setItems(llistaContactes);
    }

    @FXML
    private void btnDeleteOnAction(ActionEvent event) throws SQLException {
        Contactes c = tvContactes.getItems().get(tvContactes.getSelectionModel().getFocusedIndex());
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmar eliminar");
        alert.setHeaderText("Eliminar");
        alert.setContentText("Estas segur que vols eliminar el contacte " + c.getNom() + " ?");

        ButtonType btnSi = new ButtonType("Si");
        ButtonType btnNo = new ButtonType("No");
        tvContactes.getItems().get(tvContactes.getSelectionModel().getFocusedIndex());

        alert.getButtonTypes().setAll(btnSi, btnNo);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btnSi) {
            eliminarContacte(c.getNom());
            refresca();
        }

    }

    public ObservableList<Contactes> obtenirDades() {
        String sql = "SELECT con.codi, con.nom, car.nom, dep.nom, con.telefon1, con.telefon2, con.fax, con.correu, con.contacte_principal, con.horari1, con.horari2, cli.nom_comercial  "
                + "FROM contactes con, carrec car, departament dep, clients cli  "
                + "WHERE con.carrec=car.codi "
                + "AND con.departament=dep.codi "
                + "AND con.id_parent=cli.id_referencia";
        ArrayList<Contactes> list = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Contactes c = new Contactes();
                int contacte = 0;
                String contact = "";
                contacte = resultSet.getInt(9);

                c.setCodi(resultSet.getInt(1));
                c.setNom(resultSet.getString(2));
                c.setCarrec(resultSet.getString(3));
                c.setDepartament(resultSet.getString(4));
                c.setTelefon1(resultSet.getString(5));
                c.setTelefon2(resultSet.getString(6));
                c.setFax(resultSet.getString(7));
                c.setCorreu(resultSet.getString(8));

                if (contacte == 1) {
                    contact = "Sí";
                } else {
                    contact = "No";
                }
                c.setContacte_principal(contact);
                c.setHorari1(resultSet.getString(10));
                c.setHorari2(resultSet.getString(11));
                c.setHorari2(resultSet.getString(11));
                c.setId_parent(resultSet.getString(12));

                list.add(c);

            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ObservableList<Contactes> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }

    private void configuraColumnes() {
        colCodi.setCellValueFactory(new PropertyValueFactory<>("codi"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        colCarrec.setCellValueFactory(new PropertyValueFactory<>("carrec"));
        colDepartament.setCellValueFactory(new PropertyValueFactory<>("departament"));
        colTelefon.setCellValueFactory(new PropertyValueFactory<>("telefon1"));
        colTelefon2.setCellValueFactory(new PropertyValueFactory<>("telefon2"));
        colFax.setCellValueFactory(new PropertyValueFactory<>("fax"));
        colEmail.setCellValueFactory(new PropertyValueFactory<>("correu"));
        colContacte.setCellValueFactory(new PropertyValueFactory<>("contacte_principal"));
        colHorari1.setCellValueFactory(new PropertyValueFactory<>("horari1"));
        colHorari2.setCellValueFactory(new PropertyValueFactory<>("horari2"));
        colIdparent.setCellValueFactory(new PropertyValueFactory<>("id_parent"));
    }

    private void taulaFiltratge() {
        FilteredList<Contactes> filteredData = new FilteredList<>(tvContactes.getItems(), p -> true);
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(contactes -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                String upperCaseFilter = newValue.toUpperCase();
                if (contactes.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else if (contactes.getId_parent().indexOf(upperCaseFilter) != -1) {
                    return true;
                }
                return false;
            });
        });
        SortedList<Contactes> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tvContactes.comparatorProperty());
        tvContactes.setItems(sortedData);
    }

    public void refresca() {
        tvContactes.setItems(obtenirDades());
        taulaFiltratge();
    }

    private void eliminarContacte(String nomContacte) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM contactes WHERE nom = ?");
            preparedStatement.setString(1, nomContacte);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut eliminar el contacte", ex);
        }
    }

    @FXML
    private void btnFirstOnAction(ActionEvent event) {
        Utilitats.nBtnFirst(tvContactes);
    }

    @FXML
    private void btnRightOnAction(ActionEvent event) {
        Utilitats.nBtnRight(tvContactes);
    }

    @FXML
    private void btnLeftOnAction(ActionEvent event) {
        Utilitats.nBtnLeft(tvContactes);
    }

    @FXML
    private void btnLastOnAction(ActionEvent event) {
        Utilitats.nBtnLast(tvContactes);
    }

}
