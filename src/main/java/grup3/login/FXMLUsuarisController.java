package grup3.login;
import grup3.Usuaris;
import static grup3.classes.Utilitats.alerta_warn;
import static grup3.login.FXMLLoginController.connection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
/**
 * FXML Controller class
 *
 * @author m15
 */
public class FXMLUsuarisController implements Initializable {
    static int PRIMER = 0;
    static int POSICIO = 0;
    @FXML
    private TableColumn<Usuaris, String> colNom;
    @FXML
    private TableColumn<Usuaris, String> colUsuari;
    @FXML
    private TableColumn<Usuaris, String> colEmail;
    @FXML
    private TableColumn<Usuaris, String> colData;
    @FXML
    private TableColumn<Usuaris, Integer> colAdmin;
    @FXML
    private TableColumn<Usuaris, Integer> colBloquejat;
    @FXML
    private TableView<Usuaris> tvUsuaris;
    @FXML
    private TextField tfBuscar;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button btnFirst;
    @FXML
    private Button btnDelete;
    public static String nom;
    ObservableList<Usuaris> llistaUsuaris;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configuraColumnes();        
        refresca();
        
        tvUsuaris.getSelectionModel().select(PRIMER);
        //tvUsuaris.getFocusModel().focuUsuariss(PRIMER);
               
        if(tvUsuaris.getFocusModel().getFocusedIndex() == POSICIO) {
            btnDelete.setDisable(true);
        }
        //Navegacio amb el teclat    
        tvUsuaris.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.LEFT)) {
                if(tvUsuaris.getFocusModel().getFocusedIndex() != PRIMER) {  
                    tvUsuaris.requestFocus();
                    tvUsuaris.getSelectionModel().select(PRIMER);
                    tvUsuaris.getFocusModel().focus(PRIMER);
                }
            }
            if (event.getCode().equals(KeyCode.RIGHT)) {
                int TOTAL =  tvUsuaris.getItems().size()-1;
                if(tvUsuaris.getFocusModel().getFocusedIndex() != TOTAL) {  
                    tvUsuaris.requestFocus();
                    tvUsuaris.getSelectionModel().select(TOTAL);
                    tvUsuaris.getFocusModel().focus(TOTAL);
                }
            }
        });

        //Doble click
        tvUsuaris.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                btnDelete.setDisable(false);
                nom = newValue.getNom();
                tvUsuaris.setOnMousePressed(event -> {
                    if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLEditUsuaris.fxml"));
                            Parent root = (Parent) loader.load();
                            Stage stage = new Stage();
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.initStyle(StageStyle.DECORATED);
                            stage.setScene(new Scene(root));
                            stage.show();
                        } catch (IOException ex) {
                            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            } else {
                btnDelete.setDisable(true);
            }
        });
    }    
    
    @FXML
    private void btnDeleteOnAction(ActionEvent event) throws SQLException {
        FXMLLoginController login = new FXMLLoginController();
        Usuaris u = tvUsuaris.getItems().get(tvUsuaris.getSelectionModel().getFocusedIndex());
        if(nom.equals(login.usr)) {
            alerta_warn("Error", "Error en eliminar usuari", "No et pots eliminar a tu mateix, cap de suro!");
        }
        else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmar eliminar");
            alert.setHeaderText("Eliminar");
            alert.setContentText("Estas segur que vols eliminar l'usuari " + u.getNom() + " ?");

            ButtonType btnSi = new ButtonType("Si");
            ButtonType btnNo = new ButtonType("No");tvUsuaris.getItems().get(tvUsuaris.getSelectionModel().getFocusedIndex());

            alert.getButtonTypes().setAll(btnSi, btnNo);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == btnSi) {
                eliminarUsuari(nom);
                refresca();
            }
        }
    }

    @FXML
    private void nouUsuariAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLCrearUsuari.fxml"));
        Parent root = (Parent) loader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.setScene(new Scene(root));
        stage.show();
        
        //RNB. S'executa al tancar-se la finestra FXMLCrearUsuari.fxml
        stage.setOnHidden((WindowEvent event1) -> {
            refresca();
        });
    }

    private void configuraColumnes() {
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        colUsuari.setCellValueFactory(new PropertyValueFactory<>("usuari"));
        colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        colData.setCellValueFactory(new PropertyValueFactory<>("data_contrasenya"));
        colAdmin.setCellValueFactory(new PropertyValueFactory<>("admin"));
        colBloquejat.setCellValueFactory(new PropertyValueFactory<>("bloquejat"));
    }
    
    private ObservableList<Usuaris> obtenirDades() {        
        String sql = "SELECT usuari, nom, email, data_contrasenya, admin, bloquejat FROM login";        
        ArrayList<Usuaris> list = new ArrayList<>();        

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Usuaris u = new Usuaris();
                u.setNom(resultSet.getString(1));
                u.setUsuari(resultSet.getString(2));
                u.setEmail(resultSet.getString(3));
                u.setData_contrasenya(resultSet.getString(4));
                u.setAdmin(resultSet.getInt(5));
                u.setBloquejat(resultSet.getInt(6));
                list.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ObservableList<Usuaris> tmp = FXCollections.observableArrayList(list);
        return tmp;
    }

    private void taulaFiltratge() {        
        FilteredList<Usuaris> filteredData = new FilteredList<>(tvUsuaris.getItems(), p -> true);
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(usuaris -> {
                if(newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if(usuaris.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else if (usuaris.getUsuari().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;                 
                }
                return false;
            });
        });
        SortedList<Usuaris> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tvUsuaris.comparatorProperty());
        tvUsuaris.setItems(sortedData);
    }

    @FXML
    private void refreshAction(ActionEvent event) {
        //refresca();
    }

    private void refresca() {                
        tvUsuaris.setItems(obtenirDades());
        taulaFiltratge();
    }
    
    private void eliminarUsuari(String nomUsuari) {                
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM login WHERE usuari = ?");
            preparedStatement.setString(1, nomUsuari);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLUsuarisController.class.getName()).log(Level.SEVERE, "No s'ha pogut eliminar l'usuari", ex);            
        }        
    }
    
    @FXML
    private void btnPrimerOnAction (ActionEvent event) {
        POSICIO = 0;
        if(tvUsuaris.getFocusModel().getFocusedIndex() != POSICIO){  
            tvUsuaris.requestFocus();
            tvUsuaris.getSelectionModel().select(POSICIO);
            tvUsuaris.getFocusModel().focus(POSICIO);
        }
    }
    @FXML
    private void btnRightOnAction (ActionEvent event) {
       int TOTAL =  tvUsuaris.getItems().size();
       if(POSICIO == TOTAL - 1){
           POSICIO = TOTAL;
           POSICIO= tvUsuaris.getFocusModel().getFocusedIndex();
       }
       else {
            POSICIO= tvUsuaris.getFocusModel().getFocusedIndex();
            POSICIO++;
            tvUsuaris.requestFocus();
            tvUsuaris.getSelectionModel().select(POSICIO);
            tvUsuaris.getFocusModel().focus(POSICIO); 
        }
    }
    @FXML
    private void btnLeftOnAction (ActionEvent event) {
        int TOTAL =  tvUsuaris.getItems().size()-1;
        int PRIMER = 0;
        if (POSICIO < PRIMER) 
            POSICIO = PRIMER;
        else {
            POSICIO= tvUsuaris.getFocusModel().getFocusedIndex();
            POSICIO--;
            tvUsuaris.requestFocus();
            tvUsuaris.getSelectionModel().select(POSICIO);
            tvUsuaris.getFocusModel().focus(POSICIO);
        }
    }
    @FXML
    private void btnUltimOnAction (ActionEvent event) {
        int TOTAL =  tvUsuaris.getItems().size()-1;
        if(tvUsuaris.getFocusModel().getFocusedIndex() != TOTAL){  
            tvUsuaris.requestFocus();
            tvUsuaris.getSelectionModel().select(TOTAL);
            tvUsuaris.getFocusModel().focus(TOTAL);
        }
    }
}