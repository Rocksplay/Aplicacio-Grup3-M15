/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Vies {
    
    private final SimpleStringProperty via = new SimpleStringProperty();
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();

     public int getCodi() {
        return codi.get();
    }

    public SimpleIntegerProperty codiProperty() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi.set(codi);
    }

    public String getVia() {
        return via.get();
    }

    public SimpleStringProperty viaProperty() {
        return via;
    }

    public void setVia(String via) {
        this.via.set(via);
    }
    
    @Override
    public String toString() {
        return via.get();
    } 
}
