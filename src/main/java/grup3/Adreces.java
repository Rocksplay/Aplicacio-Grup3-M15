package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author m15
 */
public class Adreces {
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
    private final SimpleStringProperty centre = new SimpleStringProperty();
    private Vies vies = new Vies();
    private final SimpleIntegerProperty cp = new SimpleIntegerProperty();
    private final SimpleStringProperty poblacio = new SimpleStringProperty();
    private Provincies provincia = new Provincies();
    private Paisos pais = new Paisos();    
    private final SimpleStringProperty telefon = new SimpleStringProperty();
    private final SimpleStringProperty horari1 = new SimpleStringProperty();
    private final SimpleStringProperty horari2 = new SimpleStringProperty();
    private final SimpleIntegerProperty idClient = new SimpleIntegerProperty();
    //
    public Integer getCodi() {
        return codi.get();
    }
    public SimpleIntegerProperty codiProperty() {
        return codi;
    }
    public void setCodi(Integer codi) {
        this.codi.set(codi);
    }
    //
    public String getCentre() {
        return centre.get();
    }
    public SimpleStringProperty nomProperty() {
        return centre;
    }
    public void setCentre(String centre) {
        this.centre.set(centre);
    }
    //
    public Vies getVies() {
        return vies;
    }
    public void setVies(Vies via) {
        this.vies = via;
    }
    //
    public Integer getCp() {
        return cp.get();
    }
    //
    public SimpleIntegerProperty cpProperty() {
        return cp;
    }
    public void setCp(Integer cp) {
        this.cp.set(cp);
    }
    //
    public String getPoblacio() {
        return poblacio.get();
    }
    public SimpleStringProperty poblacioProperty() {
        return poblacio;
    }
    public void setPoblacio(String poblacio) {
        this.poblacio.set(poblacio);
    }
    //
    public Provincies getProvincia() {
        return provincia;
    }
    public void setProvincia(Provincies provincia) {
        this.provincia = provincia;
    }
    //
    public Paisos getPais() {
        return pais;
    }
    
    public void setPais(Paisos pais) {
        this.pais = pais;
    }
    //
    public String getTelefon() {
        return telefon.get();
    }
    public SimpleStringProperty telefonProperty() {
        return telefon;
    }
    public void setTelefon(String telefon) {
        this.telefon.set(telefon);
    }
    //
    public String getHorari1() {
        return horari1.get();
    }
    public SimpleStringProperty horari1Property() {
        return horari1;
    }
    public void setHorari1(String horari1) {
        this.horari1.set(horari1);
    }
    //
    public String getHorari2() {
        return horari2.get();
    }
    public SimpleStringProperty horari2Property() {
        return horari2;
    }
    public void setHorari2(String horari2) {
        this.horari2.set(horari2);
    }
    //
    public Integer getIdClient() {
        return idClient.get();
    }
    public SimpleIntegerProperty idClientProperty() {
        return idClient;
    }
    public void setIdClient(Integer idClient) {
        this.idClient.set(idClient);
    }
}