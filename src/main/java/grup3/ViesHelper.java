/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3;

import grup3.login.FXMLAltaAdrecesController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author infot
 */
public class ViesHelper {
    
    public static Vies obteVies(String nom, Connection connection) {
        Vies via = new Vies();        
        try {            
            PreparedStatement consulta = connection.prepareStatement("SELECT codi, nom FROM via WHERE nom = ?;");
            consulta.setString(1, nom);
            ResultSet resultado = consulta.executeQuery();            
           
            if (resultado.next()) {                                
                via.setCodi(resultado.getInt(1));
                via.setVia(resultado.getString(2));                
            }            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return via;
    }
    
}
