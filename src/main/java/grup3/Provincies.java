package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author m15
 */
public class Provincies {
    private final SimpleIntegerProperty id = new SimpleIntegerProperty();
    private final SimpleStringProperty nom = new SimpleStringProperty();
    private final SimpleStringProperty pais = new SimpleStringProperty();
    //
    public Integer getId() {
        return id.get();
    }
    public SimpleIntegerProperty idProperty() {
        return id;
    }
    public void setId(Integer id) {
        this.id.set(id);
    }
    //
    public String getNom() {
        return nom.get();
    }
    public SimpleStringProperty nomProperty() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom.set(nom);
    }
    //
    public String getPais() {
        return pais.get();
    }
    public SimpleStringProperty paisProperty() {
        return pais;
    }
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    @Override
    public String toString() {
        
        return nom.get();
    } 
}