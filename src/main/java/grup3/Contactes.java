/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author m15
 */
public class Contactes {

    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
    private final SimpleStringProperty nom = new SimpleStringProperty();
    private final SimpleStringProperty carrec = new SimpleStringProperty();
    private final SimpleStringProperty departament = new SimpleStringProperty();
    private final SimpleStringProperty telefon1 = new SimpleStringProperty();
    private final SimpleStringProperty telefon2 = new SimpleStringProperty();
    private final SimpleStringProperty fax = new SimpleStringProperty();
    private final SimpleStringProperty correu = new SimpleStringProperty();
    private final SimpleStringProperty horari1 = new SimpleStringProperty();
    private final SimpleStringProperty horari2 = new SimpleStringProperty();
    private final SimpleStringProperty contacte_principal = new SimpleStringProperty();
    private final SimpleStringProperty id_parent = new SimpleStringProperty();
    

    public int getCodi() {
        return codi.get();
    }

    public SimpleIntegerProperty codiProperty() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi.set(codi);
    }

    //
    public String getNom() {
        return nom.get();
    }

    public SimpleStringProperty nomProperty() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom.set(nom);
    }

    //
    public String getCarrec() {
        return carrec.get();
    }

    public SimpleStringProperty carrecProperty() {
        return carrec;
    }

    public void setCarrec(String carrec) {
        this.carrec.set(carrec);
    }

    //
    public String getDepartament() {
        return departament.get();
    }

    public SimpleStringProperty departamentProperty() {
        return departament;
    }

    public void setDepartament(String departament) {
        this.departament.set(departament);
    }
    //

    public String getTelefon1() {
        return telefon1.get();
    }

    public SimpleStringProperty telefon1Property() {
        return telefon1;
    }

    public void setTelefon1(String telefon1) {
        this.telefon1.set(telefon1);
    }

    //
    public String getTelefon2() {
        return telefon2.get();
    }

    public SimpleStringProperty telefon2Property() {
        return telefon2;
    }

    public void setTelefon2(String telefon2) {
        this.telefon2.set(telefon2);
    }
    //

    public String getFax() {
        return fax.get();
    }

    public SimpleStringProperty faxProperty() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax.set(fax);
    }
    //

    public String getCorreu() {
        return correu.get();
    }

    public SimpleStringProperty correuProperty() {
        return correu;
    }

    public void setCorreu(String correu) {
        this.correu.set(correu);
    }
    //

    public String gethorari1() {
        return horari1.get();
    }

    public SimpleStringProperty horari1Property() {
        return horari1;
    }

    public void setHorari1(String horari1) {
        this.horari1.set(horari1);
    }
    //

    public String gethorari2() {
        return horari2.get();
    }

    public SimpleStringProperty horari2Property() {
        return horari2;
    }

    public void setHorari2(String horari2) {
        this.horari2.set(horari2);
    }
    //

    public String getContacte_principal() {
        return contacte_principal.get();
    }

    public SimpleStringProperty contacte_principalProperty() {
        return contacte_principal;
    }

    public void setContacte_principal(String contacte_principal) {
        this.contacte_principal.set(contacte_principal);
    }

    //
    public String getId_parent() {
        return id_parent.get();
    }

    public SimpleStringProperty id_parentProperty() {
        return id_parent;
    }

    public void setId_parent(String id_parent) {
        this.id_parent.set(id_parent);
    }

}
