package grup3;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Grup3
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/view/FXMLLogin.fxml"));
        Scene scene = new Scene(root);

//PROPIETATS DE L'ESCENA
        stage.setTitle("Login - Grup3");
        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.getIcons().add(new Image ("file:icono.png"));
        stage.show();
        
    }

    public static void main(String[] args) {
        launch(args);
    }
}
