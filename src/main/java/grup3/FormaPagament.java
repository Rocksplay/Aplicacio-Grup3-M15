/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author m15
 */
public class FormaPagament {
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
    private final SimpleStringProperty nom = new SimpleStringProperty();
    private final SimpleIntegerProperty formula_dies = new  SimpleIntegerProperty();
    
    public int getCodi() {
        return codi.get();
    }

    public SimpleIntegerProperty codiProperty() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi.set(codi);
    }

    //
    public String getNom() {
        return nom.get();
    }

    public SimpleStringProperty nomProperty() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom.set(nom);
    }
      //
    public int getFormula_dies() {
        return formula_dies.get();
    }

    public SimpleIntegerProperty formula_diesProperty() {
        return formula_dies;
    }

    public void setFormula_dies(int formula_dies) {
        this.formula_dies.set(formula_dies);
    }
}
