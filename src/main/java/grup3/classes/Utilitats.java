/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3.classes;

import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;

public class Utilitats {
    private static void alerta(Alert.AlertType tipus, String titol, String header, String missatge) {
        Alert alert = new Alert(tipus);
        alert.setTitle(titol);
        alert.setHeaderText(header);
        alert.setContentText(missatge);
        alert.showAndWait();    
    }
    
    public static void alerta_info(String titol, String header, String missatge) {
        alerta(Alert.AlertType.INFORMATION, titol, header, missatge);
    }
    
    public static void alerta_warn(String titol, String header, String missatge) {
        alerta(Alert.AlertType.WARNING, titol, header, missatge);
    }
    
    public static void alerta_err(String titol, String header, String missatge) {
        alerta(Alert.AlertType.ERROR, titol, header, missatge);
    }
    
    public static void nTeclat(TableView tvDefecte) {
        tvDefecte.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.LEFT)) {
                tvDefecte.requestFocus();
                tvDefecte.getSelectionModel().selectFirst();
                tvDefecte.getFocusModel().focus(tvDefecte.getSelectionModel().getSelectedIndex());                
                tvDefecte.scrollTo(tvDefecte.getSelectionModel().getSelectedIndex());
            }
            if (event.getCode().equals(KeyCode.RIGHT)) {
                tvDefecte.requestFocus();
                tvDefecte.getSelectionModel().selectLast();
                tvDefecte.getFocusModel().focus(tvDefecte.getSelectionModel().getSelectedIndex());                
                tvDefecte.scrollTo(tvDefecte.getSelectionModel().getSelectedIndex());
            }        
        }); 
    }
    
    public static void nBtnFirst(TableView tvDefecte) {
        tvDefecte.requestFocus();
        tvDefecte.getSelectionModel().selectFirst();
        tvDefecte.getFocusModel().focus(tvDefecte.getSelectionModel().getSelectedIndex());                
        tvDefecte.scrollTo(tvDefecte.getSelectionModel().getSelectedIndex());
    }
    
    public static void nBtnLeft(TableView tvDefecte) {
        int POSICIO = tvDefecte.getSelectionModel().getFocusedIndex();
        POSICIO--;
        tvDefecte.requestFocus();
        tvDefecte.getFocusModel().focus(POSICIO);
        tvDefecte.getSelectionModel().select(POSICIO);
    }
    
    public static void nBtnRight(TableView tvDefecte) {
        if(tvDefecte.getSelectionModel().getSelectedIndex() != tvDefecte.getItems().size()) {
            int POSICIO = tvDefecte.getSelectionModel().getFocusedIndex();
            POSICIO++;
            tvDefecte.requestFocus();
            tvDefecte.getFocusModel().focus(POSICIO);
            tvDefecte.getSelectionModel().select(POSICIO);
        }
        else 
            tvDefecte.getSelectionModel().selectLast();
    }
    
    public static void nBtnLast(TableView tvDefecte) {
        tvDefecte.requestFocus();
        tvDefecte.getSelectionModel().selectLast();
        tvDefecte.getFocusModel().focus(tvDefecte.getSelectionModel().getSelectedIndex());                
        tvDefecte.scrollTo(tvDefecte.getSelectionModel().getSelectedIndex());
    }
}
