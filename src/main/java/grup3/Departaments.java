/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Departaments {
    
    private final SimpleStringProperty nomDepartament = new SimpleStringProperty();
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();

    public Departaments() {
    }

     public int getCodi() {
        return codi.get();
    }

    public SimpleIntegerProperty codiProperty() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi.set(codi);
    }

    public String getnomDepartament() {
        return nomDepartament.get();
    }

    public SimpleStringProperty nomProperty() {
        return nomDepartament;
    }

    public void setTipusVia(String tipusVia) {
        this.nomDepartament.set(tipusVia);
    }
}
