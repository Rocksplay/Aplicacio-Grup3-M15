package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author m15
 */
public class Paisos {
    private final SimpleIntegerProperty id = new SimpleIntegerProperty();
    private final SimpleStringProperty nom = new SimpleStringProperty();
    private final SimpleStringProperty codi = new SimpleStringProperty();
    //
    public Integer getId() {
        return id.get();
    }
    public SimpleIntegerProperty idProperty() {
        return id;
    }
    public void setId(Integer id) {
        this.id.set(id);
    }
    //
    public String getNom() {
        return nom.get();
    }
    public SimpleStringProperty nomProperty() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom.set(nom);
    }
    //
    public String getCodi() {
        return codi.get();
    }
    public SimpleStringProperty codiProperty() {
        return codi;
    }
    public void setCodi(String codi) {
        this.codi.set(codi);
    }

    @Override
    public String toString() {
        return nom.get();
    }    
}