/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package grup3;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class Tipus_impost {
    
    private final SimpleIntegerProperty codi = new SimpleIntegerProperty();
    private final SimpleStringProperty nom = new SimpleStringProperty();
    private final SimpleIntegerProperty valor = new SimpleIntegerProperty();

    public Tipus_impost() {
    }

    public SimpleIntegerProperty codiPropertyi() {
        return codi;
    }

    public SimpleStringProperty NomProperty() {
        return nom;
    }

    public SimpleIntegerProperty ValorpProperty() {
        return valor;
    }
    
    public int getCodi() {
        return codi.get();
    }
    
    public String getNom() {
        return nom.get();
    }
    
    public int getValor() {
        return valor.get();
    }
    
    
    public void setCodi(int codi){
    
        this.codi.set(codi);
        
    }
    
    public void setNom(String nom) {
        this.nom.set(nom);
    }
    public void setValori(int valor){
    
        this.valor.set(valor);
        
    }
    
}
