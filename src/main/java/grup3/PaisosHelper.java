/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grup3;

import grup3.login.FXMLAltaAdrecesController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author infot
 */
public class PaisosHelper {
    
    public static Paisos obtePais(String nom, Connection connection) {
        Paisos pais = new Paisos();        
        try {            
            PreparedStatement consulta = connection.prepareStatement("SELECT id, nom, codi FROM pais WHERE nom = ?;");
            
            consulta.setString(1, nom);
            ResultSet resultado = consulta.executeQuery();            
           
            if (resultado.next()) {                                
                pais.setId(resultado.getInt(1));
                pais.setNom(resultado.getString(2));                
                pais.setCodi(resultado.getString(3));                
            }            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLAltaAdrecesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pais;
    }
    
}
